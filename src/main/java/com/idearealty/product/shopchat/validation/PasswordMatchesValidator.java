package com.idearealty.product.shopchat.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;



public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

	@Override
	public void initialize(PasswordMatches constraintAnnotation) {
	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {
		IdeaRealtyUser user = (IdeaRealtyUser) obj;
		return user.getPassword().equals(user.getMatchingPassword());
	}
}
