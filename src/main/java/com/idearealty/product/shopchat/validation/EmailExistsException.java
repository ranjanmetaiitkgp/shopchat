package com.idearealty.product.shopchat.validation;

@SuppressWarnings("serial")
public class EmailExistsException extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailExistsException(String message) {
		super(message);
	}
}
