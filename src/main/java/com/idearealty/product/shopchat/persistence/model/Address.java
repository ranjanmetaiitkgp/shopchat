/**
 * 
 */
package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Debopam
 *
 */
@Document(collection="address")
@TypeAlias("address")
public class Address extends CommonDomainAttributes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8820483439856446454L;


	private String addrLine1;
	private String addrLine2;
	private String locality;
	private String city;
	private String state;
	private String country;

	/**
	 * 
	 */
	public Address() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * New Address Constructor
	 * 
	 * @param addrLine1
	 * @param addrLine2
	 * @param locality
	 * @param city
	 * @param state
	 * @param country
	 */
	public Address(String addrLine1, String addrLine2, String locality,
			String city, String state, String country) {
		super();
		this.addrLine1 = addrLine1;
		this.addrLine2 = addrLine2;
		this.locality = locality;
		this.city = city;
		this.state = state;
		this.country = country;
	}



	/**
	 * @return the addrLine1
	 */
	public String getAddrLine1() {
		return addrLine1;
	}
	/**
	 * @param addrLine1 the addrLine1 to set
	 */
	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}
	/**
	 * @return the addrLine2
	 */
	public String getAddrLine2() {
		return addrLine2;
	}
	/**
	 * @param addrLine2 the addrLine2 to set
	 */
	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}
	/**
	 * @return the locality
	 */
	public String getLocality() {
		return locality;
	}
	/**
	 * @param locality the locality to set
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Address [addrLine1=").append(addrLine1)
		.append(", addrLine2=").append(addrLine2).append(", locality=")
		.append(locality).append(", city=").append(city)
		.append(", state=").append(state).append(", country=")
		.append(country).append(", toString()=")
		.append(super.toString()).append("]");
		return builder.toString();
	}



}
