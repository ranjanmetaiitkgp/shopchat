package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="question")
@TypeAlias("question")
public class Question extends CommonDomainAttributes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Question() {
		// TODO Auto-generated constructor stub
	}

	@DBRef
	private Product product;

	@TextIndexed()
	private String questionText;

	@DBRef
	@Indexed(background=true,name="QUESTION_ANSWER",dropDups=true)
	private List<Answer> answers;

	@DBRef
	Question parentQuestion;

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the questionText
	 */
	public String getQuestionText() {
		return questionText;
	}

	/**
	 * @param questionText the questionText to set
	 */
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	/**
	 * @return the answers
	 */
	public List<Answer> getAnswers() {
		return answers;
	}

	/**
	 * @param answers the answers to set
	 */
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public void addAnswer(Answer answer) {
		if(null == getAnswers()){
			answers = new LinkedList<>();
		}
		answers.add(answer);
	}

	/**
	 * @return the parentQuestion
	 */
	public Question getParentQuestion() {
		return parentQuestion;
	}

	/**
	 * @param parentQuestion the parentQuestion to set
	 */
	public void setParentQuestion(Question parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Question [product=").append(product)
		.append(", questionText=").append(questionText)
		.append(", toString()=").append(super.toString()).append("]");
		return builder.toString();
	}


}
