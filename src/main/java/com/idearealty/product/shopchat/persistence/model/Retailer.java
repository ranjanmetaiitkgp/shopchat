package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="retailer")
@TypeAlias("retailer")
public class Retailer extends CommonDomainAttributes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2712025356211775960L;

	public Retailer() {
		// TODO Auto-generated constructor stub
	}

	@DBRef
	@Indexed(background=true,name="RETAILER_USER",dropDups=true)
	private IdeaRealtyUser user;

	@DBRef
	private List<Product> products;

	private String shopName;

	@DBRef
	private Address address;

	@DBRef
	private Retailer parentRetailer;

	/**
	 * @return the user
	 */
	public IdeaRealtyUser getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(IdeaRealtyUser user) {
		this.user = user;
	}

	/**
	 * @return the products
	 */
	public final List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public final void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the parentRetailer
	 */
	public Retailer getParentRetailer() {
		return parentRetailer;
	}

	/**
	 * @param parentRetailer the parentRetailer to set
	 */
	public void setParentRetailer(Retailer parentRetailer) {
		this.parentRetailer = parentRetailer;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Retailer [user=").append(user).append(", products=")
		.append(products).append(", shopName=").append(shopName)
		.append(", address=").append(address)
		.append(", parentRetailer=").append(parentRetailer)
		.append(", toString()=").append(super.toString()).append("]");
		return builder.toString();
	}

}
