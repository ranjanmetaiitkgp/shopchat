/**
 * 
 */
package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Debopam
 *
 */
@Document(collection="IdeaRealtyUser")
//@TypeAlias("user")
public class IdeaRealtyUser implements Serializable {



	@Id
	private String id;

	@Field("email")
	private String email;
	@Field("username")
	@Indexed(unique=true,background=true,name="IDEAREALTYUSER_USERNAME",dropDups=true)
	private String username;
	@Field("role")
	private String role;
	@Field("firstName")
	private String firstName;
	@Field("lastName")
	private String lastName;
	@Field("password")
	private String password;

	@Field("enabled")
	private boolean enabled;

	@DBRef
	private Address address;


	@Transient
	private String matchingPassword;

	@JsonIgnore
	@CreatedDate
	private Date createDate;

	@JsonIgnore
	@LastModifiedDate
	private Date lastModifiedDate;
	@JsonIgnore
	@CreatedBy
	private String createdBy;
	@JsonIgnore
	@LastModifiedBy
	private String lastModifiedBy;

	public IdeaRealtyUser(){

	}	

	/**
	 * @param username
	 * @param password
	 * @param authorities
	 */
	public IdeaRealtyUser(String username, String password,String email,
			String... authorities) {
		//super(username, password, authorities);
		this.username = username;
		this.password = password;
		this.email =email;
		this.role = StringUtils.arrayToCommaDelimitedString(authorities);
		this.enabled = true;
	}
	
	/**
	 * @param username
	 * @param password
	 * @param authorities
	 */
	public IdeaRealtyUser(String username, String password,String email,
			String role) {
		//super(username, password, authorities);
		this.username = username;
		this.password = password;
		this.email =email;
		this.role = role;
		this.enabled = true;
	}

	public IdeaRealtyUser(IdeaRealtyUser user) {
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.username = user.getUsername();
		this.email =user.getEmail();
		this.password = user.getPassword();
		this.role = user.getRole();
		this.enabled = user.isEnabled();
	}



	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}




	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.enabled = isEnabled;
	}




	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;





	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}




	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IdeaRealtyUser [id=");
		builder.append(id);
		builder.append(", email=");
		builder.append(email);
		builder.append(", username=");
		builder.append(username);
		builder.append(", role=");
		builder.append(role);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", address=");
		builder.append(address);
		builder.append(", password=");
		builder.append(password);
		builder.append(", matchingPassword=");
		builder.append(matchingPassword);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", lastModifiedDate=");
		builder.append(lastModifiedDate);
		builder.append(", createdBy=");
		builder.append(createdBy);
		builder.append(", lastModifiedBy=");
		builder.append(lastModifiedBy);
		builder.append("]");
		return builder.toString();
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdeaRealtyUser other = (IdeaRealtyUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	/*public Collection<GrantedAuthority> getAuthorities() {
		if(null == getRole() || getRole().isEmpty()){
			@SuppressWarnings("serial")
			GrantedAuthority gr = new GrantedAuthority() {
	 *//**
	 * 
	 *//*
				private static final long serialVersionUID = 1L;

				@Override
				public String getAuthority() {
					return "DUMMY";
				}
			};
			return Arrays.asList(gr);
		}
		return AuthorityUtils.commaSeparatedStringToAuthorityList(getRole());
	}*/




}
