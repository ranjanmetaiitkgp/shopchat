/**
 * 
 */
package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Debopam
 *
 */
@Document(collection="logininfo")
@TypeAlias("logininfo")
public class LoginInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8820483439856446454L;



	@Id
	private String id;
	@Indexed(background=true,name="LOGININFO_USERID",dropDups=true)
	protected String userid;
	protected String ip;
	protected String deviceinfo;
	@CreatedDate
	protected Date createDate;
	private double[] location;




	/**
	 * 
	 */
	public LoginInfo() {
		super();
	}

	/**
	 * @param userid
	 * @param ip
	 * @param deviceinfo
	 */
	public LoginInfo(String userid, String ip, String deviceinfo) {
		super();
		this.userid = userid;
		this.ip = ip;
		this.deviceinfo = deviceinfo;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the deviceinfo
	 */
	public String getDeviceinfo() {
		return deviceinfo;
	}
	/**
	 * @param deviceinfo the deviceinfo to set
	 */
	public void setDeviceinfo(String deviceinfo) {
		this.deviceinfo = deviceinfo;
	}
	/**
	 * @return the location
	 */
	public double[] getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(double[] location) {
		this.location = location;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoginInfo [id=");
		builder.append(id);
		builder.append(", userid=");
		builder.append(userid);
		builder.append(", ip=");
		builder.append(ip);
		builder.append(", deviceinfo=");
		builder.append(deviceinfo);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", location=");
		builder.append(Arrays.toString(location));
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}





}
