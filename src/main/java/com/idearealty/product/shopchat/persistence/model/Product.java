package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Debopam
 *
 */
@Document(collection="product")
@TypeAlias("product")
public class Product extends CommonDomainAttributes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String category;
	private String categoryDescription;
	//private String subCategory;
	private String productName;
	private String productDescription;


	public Product() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * 
	 * @param category
	 * @param productName
	 */
	public Product(String category, String productName) {
		super();
		this.category = category;
		this.productName = productName;
	}



	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}


	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	/**
	 * @return the categoryDescription
	 */
	public String getCategoryDescription() {
		return categoryDescription;
	}


	/**
	 * @param categoryDescription the categoryDescription to set
	 */
	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}


	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}


	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}


	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}


	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [category=").append(category)
		//.append(", subCategory=").append(subCategory)
		.append(", productName=").append(productName)
		.append(", productDescription=").append(productDescription)
		.append("]");
		return builder.toString();
	}

}
