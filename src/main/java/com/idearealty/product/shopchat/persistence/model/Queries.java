package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="queries")
@TypeAlias("queries")
public class Queries extends CommonDomainAttributes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Queries() {
		// TODO Auto-generated constructor stub
	}

	@TextIndexed
	private String query;

	@DBRef
	private List<Answer> answers;



}
