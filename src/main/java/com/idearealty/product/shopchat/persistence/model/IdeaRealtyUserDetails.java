package com.idearealty.product.shopchat.persistence.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

public class IdeaRealtyUserDetails extends IdeaRealtyUser implements UserDetails {

	public IdeaRealtyUserDetails(IdeaRealtyUser user) {
		super(user);
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		/*if(null == getRole() || getRole().isEmpty()){
			@SuppressWarnings("serial")
			GrantedAuthority gr = new GrantedAuthority() {

				private static final long serialVersionUID = 1L;

				@Override
				public String getAuthority() {
					return "DUMMY";
				}
			};
			return Arrays.asList(gr);
		}*/
		return AuthorityUtils.commaSeparatedStringToAuthorityList(getRole());
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return super.isEnabled();
	}

	private static final long serialVersionUID = 5639683223516504866L;
}
