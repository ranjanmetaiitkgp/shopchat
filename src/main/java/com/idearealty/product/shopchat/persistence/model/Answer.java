/**
 * 
 */
package com.idearealty.product.shopchat.persistence.model;

import java.io.Serializable;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * @author Debopam
 *
 */
@Document(collection="answer")
@TypeAlias("answer")
public class Answer extends CommonDomainAttributes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4195056255932348897L;

	/**
	 * 
	 */
	public Answer() {
		// TODO Auto-generated constructor stub
	}

	Question question;

	@DBRef
	@Indexed(background=true,name="ANSWER_RETAILER",dropDups=true)
	private Retailer retailer;

	@TextIndexed()
	private String answerText;


	/**
	 * @return the retailer
	 */
	public Retailer getRetailer() {
		return retailer;
	}

	/**
	 * @param retailer the retailer to set
	 */
	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}



	/**
	 * @return the answerText
	 */
	public String getAnswerText() {
		return answerText;
	}

	/**
	 * @param answerText the answerText to set
	 */
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	/**
	 * @return the question
	 */
	public final Question getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public final void setQuestion(Question question) {
		this.question = question;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Answer [question=").append(question)
		.append(", retailer=").append(retailer).append(", answerText=")
		.append(answerText).append(", toString()=")
		.append(super.toString()).append("]");
		return builder.toString();
	}
}
