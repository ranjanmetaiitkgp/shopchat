package com.idearealty.product.shopchat.listener;

import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.idearealty.product.shopchat.event.OnQuestionSubmitEvent;
import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.service.IUserService;

@Component
public class QuestionSubmissionListener implements ApplicationListener<OnQuestionSubmitEvent> {
	@Autowired
	private IUserService service;

	@Autowired
	private MessageSource messages;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private Environment env;

	@Autowired
	@Qualifier("questiontwmplate")
	private SimpleMailMessage emailTemplate;

	// API

	@Override
	public void onApplicationEvent(OnQuestionSubmitEvent event) {
		try {
			if(null != event.getUser() && null != event.getUser().getEmail() && !"".equalsIgnoreCase(event.getUser().getEmail().trim()))
				this.questionSubmission(event);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private void questionSubmission(OnQuestionSubmitEvent event) throws MessagingException {
		IdeaRealtyUser user = event.getUser();

		//service.createVerificationTokenForUser(user, token);
		Question question = event.getQuestion();
		for(Answer answer:question.getAnswers()){
			String token = UUID.randomUUID().toString();
			final MimeMessage email = constructEmailMessage(event, user, token,answer,question);

			mailSender.send(email);
		}
	}

	//

	private final MimeMessage constructEmailMessage(final OnQuestionSubmitEvent event, final IdeaRealtyUser user, final String token, Answer answer, Question question) throws MessagingException {

		final String confirmationUrl = event.getAppUrl() + "/anonymqestionanswer.html?token=" + token+"&answerid="+answer.getId();
		System.out.println("confirmationUrl :"+confirmationUrl);

		//final SimpleMailMessage email = new SimpleMailMessage();

		MimeMessage message = mailSender.createMimeMessage();
		// use the true flag to indicate you need a multipart message
		MimeMessageHelper email = new MimeMessageHelper(message, true);

		email.setTo(answer.getRetailer().getUser().getEmail());
		email.setSubject(String.format(emailTemplate.getSubject(), user.getFirstName()+" "+user.getLastName(),question.getProduct().getProductName()));
		email.setText(String.format(emailTemplate.getText(), question.getQuestionText(),confirmationUrl));
		email.setFrom(emailTemplate.getFrom());
		return message;
	}

}
