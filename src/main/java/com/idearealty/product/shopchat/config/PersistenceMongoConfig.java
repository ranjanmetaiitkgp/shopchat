package com.idearealty.product.shopchat.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.idearealty.product.shopchat.util.MongoAuditAware;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
//@EnableTransactionManagement
@PropertySource({ "classpath:persistence.properties" })
//@ComponentScan(basePackages={ "com.idearealty.product.shopchat.persistence.model" })
//@EnableJpaRepositories(basePackages = {"com.idearealty.product.shopchat.repository.authentication","com.idearealty.product.shopchat.repository.db"})
@EnableMongoRepositories(basePackages = "com.idearealty.product.shopchat.repository.mongodb")
@EnableMongoAuditing
@ComponentScan
public class PersistenceMongoConfig extends AbstractMongoConfiguration{

	@Autowired
	private Environment env;

	public PersistenceMongoConfig() {
		super();
	}

	@Bean(name="ideaRealtyAudit")
	public AuditorAware<String> mongoAuditProvider() {
		return new MongoAuditAware();
	}



	@Override
	protected String getDatabaseName() {
		return "shopchatdb";
	}

	@Override
	public Mongo mongo() throws Exception {
		MongoCredential mongoCredential = MongoCredential.createCredential("shopchatdbuser", "shopchatdb", "_sh0pChat$$".toCharArray());
		return new MongoClient(new ServerAddress("ds041693.mongolab.com", 41693),Arrays.asList(mongoCredential),mongoClientOptions());
	}

	@Bean
	public MongoClientOptions mongoClientOptions() {
		// override to more reasonable connection timeout (default is 10 seconds)
		return MongoClientOptions.builder().connectTimeout(5000)
				.connectionsPerHost(8)
				.maxWaitTime(5000)
				.socketKeepAlive(true)
				.build();
	}

	@Override
	protected String getMappingBasePackage() {
		return "com.idearealty.product.shopchat.persistence.model";
	}

}
