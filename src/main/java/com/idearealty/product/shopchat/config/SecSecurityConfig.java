package com.idearealty.product.shopchat.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.idearealty.product.shopchat.security.AuthFailureHandler;
import com.idearealty.product.shopchat.security.HttpAuthenticationEntryPoint;
import com.idearealty.product.shopchat.security.HttpLogoutSuccessHandler;

@Configuration
@ComponentScan(basePackages = { "com.idearealty.product.shopchat.security" })
@EnableWebSecurity(debug=true)
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true,order=Ordered.HIGHEST_PRECEDENCE)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private AuthenticationSuccessHandler myAuthenticationSuccessHandler;

	@Autowired
	private HttpAuthenticationEntryPoint authenticationEntryPoint;

	//@Autowired
	//private AuthSuccessHandler authSuccessHandler;


	@Autowired
	private AuthFailureHandler authFailureHandler;
	@Autowired
	private HttpLogoutSuccessHandler logoutSuccessHandler;

	public SecSecurityConfig() {
		super();
	}


	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	/*@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}*/

	/*@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}*/

	@Override
	public void configure(final WebSecurity web) throws Exception {
		DefaultWebSecurityExpressionHandler d = new DefaultWebSecurityExpressionHandler();
		//d.setPermissionEvaluator(permissionEvaluator);
		web.debug(true).expressionHandler(d).ignoring().antMatchers("/resources/**");
	}

	/*@Bean
	public MethodSecurityExpressionHandler createExpressionHandler() {
		// ... create and return custom MethodSecurityExpressionHandler ...
		return new DefaultMethodSecurityExpressionHandler();
	}
	 */

	@Override
	protected void configure(final HttpSecurity http) throws Exception {

		// @formatter:off
		http
		.csrf().disable()
		//.and()
		.authorizeRequests()
		.antMatchers("/login*", "/logout*", "/signin/**", "/signup/**",
				"/user/registration*", "/regitrationConfirm*", "/user/regitrationConfirm*", "/expiredAccount*", "/registration*","/otpverification*","/user/otpverification*",
				"/badUser*", "/user/resendRegistrationToken*" ,"/forgetPassword*", "/user/resetPassword*","/user/regenerateOtp*",
				"/user/changePassword*", "/emailError*", "/resources/**","/successRegister*","/anonymqestionanswer*").permitAll()
				.antMatchers("/invalidSession*").anonymous()
				.antMatchers("/admin","/admin/**").fullyAuthenticated()//.hasAnyRole("ADMIN")
				.antMatchers("/admin","/admin/**").fullyAuthenticated()//.hasAuthority("ADMIN")
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage("/login.html")
				.loginProcessingUrl("/login")
				.defaultSuccessUrl("/homepage.html")
				.failureUrl("/login.html?error=true")
				.successHandler(myAuthenticationSuccessHandler)
				//.successHandler(authSuccessHandler)
				.failureHandler(authFailureHandler)
				.usernameParameter("username")
				.passwordParameter("password")
				.permitAll()
				.and()
				/*.sessionManagement()
				.invalidSessionUrl("/invalidSession.html")
				.sessionFixation().newSession()
				.and()*/
				.logout()
				.invalidateHttpSession(false)
				.logoutUrl("/logout")
				.logoutSuccessUrl("/logout.html?logSucc=true")
				.deleteCookies("JSESSIONID")
				.permitAll();


		// @formatter:on
	}

	// beans

	@Bean
	public DaoAuthenticationProvider authProvider() {
		final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		//authProvider.setPasswordEncoder(encoder());
		return authProvider;
	}
	/*
	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder(11);
	}
	 */
}