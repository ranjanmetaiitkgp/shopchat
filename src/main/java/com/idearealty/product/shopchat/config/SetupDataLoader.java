package com.idearealty.product.shopchat.config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.AddressRepository;
import com.idearealty.product.shopchat.repository.mongodb.ProductRepository;
import com.idearealty.product.shopchat.repository.mongodb.UserRepository;
import com.idearealty.product.shopchat.service.RetailerService;
import com.idearealty.product.shopchat.validation.EmailExistsException;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	private boolean alreadySetup = true;

	@Autowired
	private UserRepository userRepository;

	//@Autowired
	//private PasswordEncoder passwordEncoder;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	AddressRepository addressRepository;

	@Autowired
	RetailerService retailerService;


	// API

	@Override
	//@Transactional
	public void onApplicationEvent(final ContextRefreshedEvent event) {
		if (alreadySetup) {
			return;
		}

		Address address = new Address();
		address.setCity("Kolkata");
		address.setLocality("Behala");

		Address addr = addressRepository.saveOrUpdateAddress(address);
		System.out.println("address :"+address);
		System.out.println("addr :"+addr);

		if(null == userRepository.findByUsername("9674642375")){

			final IdeaRealtyUser user = new IdeaRealtyUser("9674642375","","debopam.poddar@gmail.com","ADMIN");
			user.setId(null);
			user.setFirstName("Admin");
			user.setLastName("User");
			user.setEmail("test@test.com");
			user.setAddress(addr);
			user.setEnabled(true);
			
			userRepository.save(user);
		}else{
			alreadySetup = true;
		}

		if(!alreadySetup){
			productRepository.save(buildProductCatalogue());
		}


		if(!alreadySetup){
			try {
				retailerService.createReatiler(buildReatailer());
			} catch (EmailExistsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	private List<Retailer> buildReatailer() {
		List<Retailer> retailers = new LinkedList<>();

		List<Product> kitcheProducts = productRepository.findByCategory("Kitchen Appliance");
		List<Product> electronicsProducts = productRepository.findByCategory("Elctronics");
		List<Product> homeProducts = productRepository.findByCategory("Home & Furniture");


		Retailer kolkataRetailer = new Retailer();
		kolkataRetailer.setUser(new IdeaRealtyUser("9874654323", "", "ideatoreatly@gmail.com","RETAILER"));
		kolkataRetailer.setAddress(new Address("JadavPur Purba Para","","Jadavpur","Kolkata","West Bengal","India"));
		kolkataRetailer.setShopName("Jadabvpur Deperatmental Stores");
		List<Product> kolkataRetailerProducts= new ArrayList<>(kitcheProducts.subList(0, 2));
		kolkataRetailerProducts.addAll(electronicsProducts.subList(1, 2));
		kolkataRetailer.setProducts(kolkataRetailerProducts);

		retailers.add(kolkataRetailer);

		Retailer kolkataRetailer1 = new Retailer();
		kolkataRetailer1.setUser(new IdeaRealtyUser("9874654334", "", "ideatoreatly@gmail.com","RETAILER"));
		kolkataRetailer1.setAddress(new Address("Behala Parnasree","","Behala","Kolkata","West Bengal","India"));
		kolkataRetailer1.setShopName("Behala Home Town");
		List<Product> kolkataRetailerProducts1= new ArrayList<>(kitcheProducts.subList(0, 2));
		kolkataRetailerProducts1.addAll(electronicsProducts.subList(2, 3));
		kolkataRetailerProducts1.addAll(homeProducts);

		kolkataRetailer1.setProducts(kolkataRetailerProducts1);


		Retailer kolkataRetailer2 = new Retailer();
		kolkataRetailer2.setUser(new IdeaRealtyUser("9004654334", "","ideatoreatly@gmail.com", "RETAILER"));
		kolkataRetailer2.setAddress(new Address("Behala 14 Number","","Behala","Kolkata","West Bengal","India"));
		kolkataRetailer2.setShopName("Behala Electronics Store");
		List<Product> kolkataRetailerProducts2= new ArrayList<>(kitcheProducts.subList(2, 3));
		kolkataRetailerProducts2.addAll(electronicsProducts);

		kolkataRetailer2.setProducts(kolkataRetailerProducts2);

		retailers.add(kolkataRetailer2);


		Retailer bloreRetailer = new Retailer();
		bloreRetailer.setUser(new IdeaRealtyUser("7874954323", "", "ideatoreatly@gmail.com","RETAILER"));
		bloreRetailer.setAddress(new Address("Somewhere in Blore","","Test Blore","Bangalore","Karnataka","India"));
		bloreRetailer.setShopName("Bangalore Electronics");
		List<Product> bloreRetailerProducts= new ArrayList<>();
		bloreRetailerProducts.addAll(electronicsProducts);
		bloreRetailer.setProducts(bloreRetailerProducts);

		retailers.add(bloreRetailer);

		Retailer bloreRetailer2 = new Retailer();
		bloreRetailer2.setUser(new IdeaRealtyUser("8076578231", "", "ideatoreatly@gmail.com","RETAILER"));
		bloreRetailer2.setAddress(new Address("Somewhere in Blore","","Test Blore 2","Bangalore","Karnataka","India"));
		bloreRetailer2.setShopName("B'lore Home Furnishing");
		List<Product> bloreRetailerProducts2= new ArrayList<>(kitcheProducts.subList(0, 2));
		bloreRetailerProducts2.addAll(homeProducts.subList(3, 4));
		bloreRetailerProducts2.addAll(electronicsProducts.subList(1, 3));

		bloreRetailer2.setProducts(bloreRetailerProducts2);

		retailers.add(bloreRetailer2);




		return retailers;
	}

	private List<Product> buildProductCatalogue(){
		List<Product> products = new LinkedList<>();

		products.add(new Product("Kitchen Appliance","Electric Kettle"));
		products.add(new Product("Kitchen Appliance","Electric Rice Cooker"));
		products.add(new Product("Kitchen Appliance","Griller & Toaster"));
		products.add(new Product("Kitchen Appliance","Induction Oven"));
		products.add(new Product("Kitchen Appliance","Micro Oven"));
		products.add(new Product("Kitchen Appliance","Mixer"));
		products.add(new Product("Elctronics","Television"));
		products.add(new Product("Elctronics","Mobiles"));
		products.add(new Product("Elctronics","Tablets"));
		products.add(new Product("Elctronics","Iron"));
		products.add(new Product("Elctronics","Grooming Kit"));
		products.add(new Product("Elctronics","Large Appliances"));
		products.add(new Product("Home & Furniture","Storage"));
		products.add(new Product("Home & Furniture","Dinning"));
		products.add(new Product("Home & Furniture","Bed & Living"));
		products.add(new Product("Home & Furniture","Bath Essentials"));
		products.add(new Product("Home & Furniture","Home Decor"));
		products.add(new Product("Home & Furniture","Lighting"));
		products.add(new Product("Home & Furniture","Tools"));
		products.add(new Product("Home & Furniture","Photo Frames"));
		products.add(new Product("Clothing","Men"));
		products.add(new Product("Clothing","Women"));
		products.add(new Product("Clothing","Baby"));

		return products;
	}

	/*
	@Transactional
	private final Privilege createPrivilegeIfNotFound(final String name) {
		Privilege privilege = privilegeRepository.findByName(name);
		if (privilege == null) {
			privilege = new Privilege(name);
			privilegeRepository.save(privilege);
		}
		return privilege;
	}

	@Transactional
	private final Role createRoleIfNotFound(final String name, final List<Privilege> privileges) {
		Role role = roleRepository.findByName(name);
		if (role == null) {
			role = new Role(name);
			role.setPrivileges(privileges);
			roleRepository.save(role);
		}
		return role;
	}
	 */
}