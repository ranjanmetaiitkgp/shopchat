package com.idearealty.product.shopchat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.repository.mongodb.AddressRepository;

@Service
public class AddressService {

	@Autowired
	AddressRepository addressRepository;

	public AddressService() {
		// TODO Auto-generated constructor stub
	}

	public List<String> getCities(){
		return addressRepository.findCities();
	}

	public Address saveOrUpdateAddress(Address address){
		return addressRepository.saveOrUpdateAddress(address);
	}

	public List<String> getLocalities(String city){
		return addressRepository.findLocalities(city);
	}
}
