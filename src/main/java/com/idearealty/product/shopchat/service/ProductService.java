/**
 * 
 */
package com.idearealty.product.shopchat.service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.repository.mongodb.CustomProductRepository;
import com.idearealty.product.shopchat.repository.mongodb.ProductRepository;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.mysema.codegen.StringUtils;

/**
 * @author Debopam
 *
 */
@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	CustomProductRepository customProductRepository;

	@Autowired
	RetailerRepository retailerRepository;

	/**
	 * 
	 */
	public ProductService() {
		// TODO Auto-generated constructor stub
	}

	public List<String> getProductCategories(){
		return customProductRepository.findDistinctCategory();
	}

	public List<Product> getProductsByCategory(String category){
		return productRepository.findByCategory(category);
	}

	public List<Product> getProducts(String productIds){
		Iterable<Product> products = productRepository.findAll(Arrays.asList(productIds.split(",")));
		return Lists.newArrayList(products);
	}




}
