/**
 * 
 */
package com.idearealty.product.shopchat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.CustormRetailerRepositiry;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.idearealty.product.shopchat.validation.EmailExistsException;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

/**
 * @author Debopam
 *
 */
@Service
public class RetailerService {

	@Autowired
	RetailerRepository retailerRepository;

	@Autowired
	CustormRetailerRepositiry custormRetailerRepositiry;

	@Autowired
	UserService userService;

	@Autowired
	AddressService addressService;

	/**
	 * 
	 */
	public RetailerService() {
		// TODO Auto-generated constructor stub
	}

	public Retailer changeAddress(String id,Address addresses) throws IdeaRealtyException{
		Retailer retailer = retailerRepository.findOne(id);
		if(null == retailer){
			throw new IdeaRealtyException("Retailer not found");
		}

		retailer.setAddress(addresses);
		retailerRepository.save(retailer);
		return null;
	}

	public List<Question> findAllAnswersByReatiler(){
		return null;
	}

	public Retailer createReatiler(Retailer retailer) throws EmailExistsException{

		if(null != retailer.getAddress() && retailer.getAddress().getId().isEmpty()){
			addressService.saveOrUpdateAddress(retailer.getAddress());
		}

		if(null != retailer.getUser() && retailer.getUser().getId().isEmpty()){
			userService.registerNewUserAccount(retailer.getUser());
		}
		
		if(null != retailer.getId()){
			retailer = retailerRepository.save(retailer);
		}else{
			retailer = retailerRepository.insert(retailer);
		}

		return retailer;
	}

	public List<Retailer> createReatiler(List<Retailer> retailers) throws EmailExistsException{

		for(Retailer retailer:retailers){

			if(null != retailer.getAddress()){
				retailer.setAddress(addressService.saveOrUpdateAddress(retailer.getAddress()));
			}

			if(null != retailer.getUser()){
				retailer.setUser(userService.createOrUpdateUser(retailer.getUser()));
			}

			if(null != retailer.getId()){
				retailerRepository.save(retailer);
			}else{
				retailerRepository.insert(retailer);
			}
		}

		return retailers;
	}


	public List<Retailer> getRetailersByProductidAndLocation(String productId,String city){
		return retailerRepository.findRetailersByProductIdAndLocation(productId, city);
	}

	public List<Retailer> getRetailersByProductNameAndLocation(String productName,String city){
		return custormRetailerRepositiry.findRetailersByProductNameAndCity(productName, city);
	}
	
	public Retailer insertRetailer(Retailer retailer){
		return retailerRepository.insert(retailer);
	}

}
