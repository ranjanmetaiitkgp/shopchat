package com.idearealty.product.shopchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.repository.mongodb.CustomQuestionRepository;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.idearealty.product.shopchat.repository.mongodb.UserRepository;
import com.idearealty.product.shopchat.validation.EmailExistsException;

@Service
public class UserService implements IUserService {
	@Autowired
	private UserRepository repository;

	@Autowired
	private CustomQuestionRepository customQuestionRepository;

	@Autowired
	private RetailerRepository retailerRepository;

	//@Autowired
	//private PasswordEncoder passwordEncoder;

	// API

	@Override
	public IdeaRealtyUser registerNewUserAccount(final IdeaRealtyUser user) throws EmailExistsException {
		if (userExist(user)) {
			throw new EmailExistsException("There is an account with that email adress: " + user.getEmail());
		}

		if(null == user.getRole() || "".equals(user.getRole()))
			user.setRole("ROLE_USER");
		return repository.insert(user);
	}

	@Override
	public IdeaRealtyUser createOrUpdateUser(IdeaRealtyUser user){
		if (userExist(user)) {
			return repository.save(user);
		}else{
			return repository.insert(user);
		}
	}

	@Override
	public void deleteUser(final IdeaRealtyUser user) {
		repository.delete(user);
	}

	@Override
	public IdeaRealtyUser findUserByEmail(final String email) {
		return repository.findByEmail(email);
	}


	@Override
	public IdeaRealtyUser getUserByID(final String id) {
		return repository.findOne(id);
	}

	private boolean userExist(final IdeaRealtyUser inputUser) {
		if(null != inputUser.getEmail() && ! inputUser.getEmail().isEmpty()){
			IdeaRealtyUser user = repository.findByEmail(inputUser.getEmail());
			if (user != null) {
				return true;
			}
		}

		IdeaRealtyUser user = repository.findByUsername(inputUser.getUsername());

		if (user != null) {
			return true;
		}

		return false;
	}




	@Override
	public void saveRegisteredUser(IdeaRealtyUser user) {
		repository.save(user);
	}



	@Override
	public void deleteUser(User user) {
		// TODO Auto-generated method stub

	}

	public Page<Question> userNotifications(Pageable pageable){	
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();		
		return customQuestionRepository.findUsersQuestionsAnsweredSinceLastLogin(userDetails.getUsername(), pageable);

	}

	//@PreAuthorize("hasAnyRole('RETAILER','ADMIN','ROLE_ADMIN')")
	public Page<Answer> retailerNotifcations(Pageable pageable){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();	

		return customQuestionRepository.findQuestionsAwaitingRetailerAnswer(userDetails, pageable);			
	}



	@Override
	public IdeaRealtyUser findUserByUsername(String userName) {
		return repository.findByUsername(userName);
	}

}