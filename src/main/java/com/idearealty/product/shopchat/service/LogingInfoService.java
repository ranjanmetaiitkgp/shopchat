/**
 * 
 */
package com.idearealty.product.shopchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.LoginInfo;
import com.idearealty.product.shopchat.repository.mongodb.LoginInfoRepository;

/**
 * @author Debopam
 *
 */
@Service
public class LogingInfoService {
	@Autowired
	LoginInfoRepository loginInfoRepository;

	/**
	 * 
	 */
	public LogingInfoService() {
		// TODO Auto-generated constructor stub
	}

	public void saveLoginInfo(LoginInfo loginInfo){
		loginInfoRepository.save(loginInfo);
	}

}
