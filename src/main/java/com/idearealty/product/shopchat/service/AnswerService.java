/**
 * 
 */
package com.idearealty.product.shopchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.repository.mongodb.AnswerRepository;

/**
 * @author Debopam
 *
 */
@Service
public class AnswerService {
	@Autowired
	AnswerRepository answerRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	public Answer submitAnswer(String answerId,String answerText){
		Answer answer = answerRepository.findOne(answerId);
		answer.setAnswerText(answerText);
		return answerRepository.save(answer);
	}

	/*public Answer submitAnswer(String answerId,String answerText){

		Query query = new Query(Criteria.where("id").is(new ObjectId(answerId)));

		Update update = new Update().set("answerText", answerText);
		Answer answer = mongoTemplate.findAndModify(query, update, Answer.class); // return's old person object

		return answer;
	}*/



}
