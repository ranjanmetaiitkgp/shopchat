package com.idearealty.product.shopchat.service;

import org.springframework.security.core.userdetails.User;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.validation.EmailExistsException;

public interface IUserService {

	IdeaRealtyUser registerNewUserAccount(IdeaRealtyUser accountDto) throws EmailExistsException;

	IdeaRealtyUser createOrUpdateUser(IdeaRealtyUser accountDto);

	void deleteUser(User user);

	IdeaRealtyUser findUserByEmail(String email);

	IdeaRealtyUser findUserByUsername(String userName);

	void deleteUser(IdeaRealtyUser user);

	IdeaRealtyUser getUserByID(String id);

	void saveRegisteredUser(IdeaRealtyUser user);


}
