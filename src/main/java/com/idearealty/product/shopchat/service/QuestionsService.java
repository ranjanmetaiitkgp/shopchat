package com.idearealty.product.shopchat.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.AnswerRepository;
import com.idearealty.product.shopchat.repository.mongodb.CustomQuestionRepository;
import com.idearealty.product.shopchat.repository.mongodb.ProductRepository;
import com.idearealty.product.shopchat.repository.mongodb.QuestionRepository;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

@Service
public class QuestionsService {

	@Autowired
	QuestionRepository questionRepository;

	@Autowired
	private CustomQuestionRepository customQuestionRepository;

	@Autowired
	RetailerRepository retailerRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	AnswerRepository answerRepository;

	public QuestionsService() {

	}

	/**
	 * Insert or Edit questions
	 * 
	 * @param question
	 * @param productId
	 * @param retailerIds
	 * @return
	 * @throws IdeaRealtyException
	 */
	public Question submitQustions(Question question,String productId,List<String> retailerIds) throws IdeaRealtyException{

		Question existingQuestion = null == question.getId() || "".equalsIgnoreCase(question.getId().trim())?null: questionRepository.findOne(question.getId());

		if(null == existingQuestion){ //New Question
			List<Answer> answers = new LinkedList<>();

			Product product = productRepository.findOne(productId);
			if(null == product){
				List<Product> products = productRepository.findByProductName(productId);
				if(null != products && !products.isEmpty()){
					product = products.get(0);
				}

				if(null == product){
					throw new IdeaRealtyException("Existing product must be selected to ask a question");
				}
			}

			question.setProduct(product);

			for(String retailerId:retailerIds){
				Retailer retailer = retailerRepository.findOne(retailerId);
				if(null != retailer){
					Answer answer = new Answer();
					answer.setRetailer(retailer);
					//answer.setQuestion(question);
					answer.setAnswerText("");
					answers.add(answer);
				}
			}

			answerRepository.insert(answers);
			question.setAnswers(answers);
			questionRepository.insert(question);

		}else{
			existingQuestion.setProduct(question.getProduct()); //In case product changes
			existingQuestion.setQuestionText(question.getQuestionText());
			questionRepository.save(existingQuestion);
		}

		return question;
	}

	//@Secured({"ADMIN"})
	public Page<Question> getQuestionsByQuestionText(String searchText,Pageable pageable){
		String[] queryTexts = searchText.split(",");
		return customQuestionRepository.searchQuestionsByText(queryTexts, pageable, "");
	}

	//@Secured({"ADMIN"})
	public Page<Question> getQuestions(Pageable pageable){
		Page<Question> questions = questionRepository.findAll(pageable);
		return questions;
	}

	//@Secured({"ADMIN"})
	public Question addAnswer(String questionId,String answerText){
		Question question = questionRepository.findOne(questionId);
		Answer answer = new Answer();
		answer.setAnswerText(answerText);
		answer = answerRepository.insert(answer);
		question.addAnswer(answer);
		return questionRepository.save(question);
	}

	/**
	 * Find All questions submitted by an user
	 * @param userID
	 * @return
	 */
	public Page<Question> getQuestionsByUserId(String userid,Pageable pageable){
		Page<Question> questions = questionRepository.findQuestionsByCreatedBy(userid, pageable);
		return questions;
	}


	public Question getQuestion(String questionId){
		return questionRepository.findOne(questionId);
	}

	public Question getQuestionForAnswerId(String answerId) throws IdeaRealtyException{
		return customQuestionRepository.getQuestionForAnswerId(answerId);

	}
}
