package com.idearealty.product.shopchat.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.OtpSmsReport;
import com.idearealty.product.shopchat.repository.mongodb.OtpSmsReportRepository;
import com.idearealty.product.shopchat.util.GetRequestManager;
import com.idearealty.product.shopchat.util.SmsAgent;

@Service
public class OtpSmsService {

	@Autowired
	OtpSmsReportRepository otpSmsReportRepository;

	public Map<String, String> createSmsReportAndSendOtp(String phoneNumber){
		List<OtpSmsReport> reports = otpSmsReportRepository.findByPhoneNumberAndActiveIsTrue(phoneNumber);
		if(null != reports && ! reports.isEmpty()){
			for(OtpSmsReport report:reports){
				report.setActive(false);
			}
			otpSmsReportRepository.save(reports);
		}
		Map<String, String> responseMap = new HashMap<String, String>(); 
		SmsAgent smsAgent = new SmsAgent();
		String otp = generateOTP();
		String responseJson = sendOtpSms(phoneNumber,smsAgent,otp);
		if(!"".equalsIgnoreCase(responseJson)){
			smsAgent.setResponseJson(responseJson);
			String otpStatus = smsAgent.getStatusFromResponseJson(); 
			if( otpStatus.equals("OK")){
				boolean flag = smsAgent.extractDetailsFromResponseJson();
				if(flag){
					OtpSmsReport otpSmsReport = new OtpSmsReport();
					otpSmsReport.setPhoneNumber(smsAgent.getPhoneNumber());
					otpSmsReport.setAgentSmsId(smsAgent.getSmsId());
					otpSmsReport.setAgentGroupId(smsAgent.getGroupId());
					otpSmsReport.setMessageStatus(smsAgent.getSmsStatus());
					otpSmsReport.setVerificationStatus("Not Verified");
					otpSmsReport.setActive(true);
					otpSmsReport.setOtpCode(otp);
					otpSmsReport.setCreatedAt(new Date());
					otpSmsReport.setUpdatedAt(null);
					otpSmsReport.setVerifiedAt(null);
					try{						
						otpSmsReportRepository.save(otpSmsReport);
					}catch(Exception e){
						e.printStackTrace();
					}

					responseMap.put("status", "OK");
					responseMap.put("Reason", "SMS successfully sent.");
					responseMap.put("sms_id", smsAgent.getSmsId().toString());

				}else{
					responseMap.put("status", "FAILED");
					responseMap.put("Reason", "SMS data parsing error. Agent Error");
					responseMap.put("sms_id", "");
				}

			}else{
				System.out.println("###########here#######");
				responseMap.put("status", "FAILED");
				responseMap.put("Reason", "SMS could not be sent. Check request details");
				responseMap.put("sms_id", "");
			}
		}else{
			System.out.println("###########here123#######");
			responseMap.put("status", "FAILED");
			responseMap.put("Reason", "OTP SMS could not be sent. Check request details");
			responseMap.put("sms_id", "");
		}


		return responseMap;
	}

	private String sendOtpSms(String phoneNumber,SmsAgent mSmsAgent, String otp){

		System.out.println("Sending SMS to phone : " + phoneNumber);

		String message = getOtpMessage(otp);
		mSmsAgent.setPhoneNumber(phoneNumber);
		mSmsAgent.setMessage(message);

		URIBuilder uri = mSmsAgent.getSmsUri();

		System.out.println("URI : " + uri.toString());
		GetRequestManager mGetRequestManager = new GetRequestManager();
		mGetRequestManager.setUri(uri.toString());
		Boolean success = mGetRequestManager.sendGetRequestFromUri();

		if(success){
			return mGetRequestManager.getResponseJson();

		}
		return "";
	}

	private String getOtpMessage(String otp) {
		return String.format("Your OTP is %s", otp);
	}

	private static String generateOTP() {
		StringBuilder generatedToken = new StringBuilder();
		try {
			SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
			// Generate 20 integers 0..20
			for (int i = 0; i < 6; i++) {
				generatedToken.append(number.nextInt(9));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return generatedToken.toString();
	}

	public boolean verifyOtp(String phoneNumber,String otp){
		OtpSmsReport otpSmsReport = otpSmsReportRepository.findByPhoneNumberAndOtpCodeAndActiveIsTrue(phoneNumber, otp);
		if(null == otpSmsReport){
			return false;
		}

		otpSmsReport.setVerificationStatus("verified");
		otpSmsReport.setVerifiedAt(new Date());
		otpSmsReportRepository.save(otpSmsReport);

		return true;
	}


}
