package com.idearealty.product.shopchat.repository.mongodb;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

//@NoRepositoryBean
@Repository
public interface CustomQuestionRepository {

	public Page<Question> findUsersQuestionsAnsweredSinceLastLogin(String userid, Pageable pageable);

	public Page<Answer> findQuestionsAwaitingRetailerAnswer(UserDetails userDetails, Pageable pageable);


	public Page<Question> searchQuestionsByText(String[] text, Pageable pageable,String userid);

	public Question getQuestionForAnswerId(String answerId) throws IdeaRealtyException;

}
