/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * @author Debopam
 *
 */
@Repository
public interface CustomProductRepository {

	public List<String> findDistinctCategory();

}
