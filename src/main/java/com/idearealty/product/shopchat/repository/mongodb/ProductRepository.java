/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Product;

/**
 * @author Debopam
 *
 */
@Repository
public interface ProductRepository  extends MongoRepository<Product, String>{

	@Query(value="{ 'category' : ?0 }")
	public List<Product> findByCategory(String category);

	@Query(value="{ 'productName' : ?0 }")
	public List<Product> findByProductName(String productName);
}
