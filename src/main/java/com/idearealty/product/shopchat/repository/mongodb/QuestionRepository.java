/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Question;

/**
 * @author Debopam
 *
 */
@Repository
public interface QuestionRepository extends MongoRepository<Question, String> {

	public Page<Question> findQuestionsByCreatedBy(String userid, Pageable pageable);

	/*@Query(fields="{ 'id' : 1, 'questionText' : 1}")
	Page<Question> findAll(Pageable pageable);*/
}
