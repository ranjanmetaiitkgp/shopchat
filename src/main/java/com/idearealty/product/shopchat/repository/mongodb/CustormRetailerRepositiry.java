package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import com.idearealty.product.shopchat.persistence.model.Retailer;

public interface CustormRetailerRepositiry {

	public List<Retailer> findRetailersByProductNameAndCity(String productName,String city);
}
