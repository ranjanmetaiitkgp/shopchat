/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Address;

/**
 * @author Debopam
 *
 */
@Repository
public interface AddressRepository{

	public List<String> findCities();

	public Address saveOrUpdateAddress(Address address);

	public List<Address> findByLocality(String locality);

	List<String> findLocalities(String city); 

}
