/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.LoginInfo;

/**
 * @author Debopam
 *
 */
@Repository
public interface LoginInfoRepository extends MongoRepository<LoginInfo, String> {

	@Query(value="{'userid' : ?0}")
	public Page<LoginInfo> findByUserIDOrderByCreateDateDesc(final String userid,Pageable pageable);

}
