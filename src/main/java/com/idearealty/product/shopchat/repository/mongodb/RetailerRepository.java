/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.persistence.model.Retailer;

/**
 * @author Debopam
 *
 */
@Repository
public interface RetailerRepository extends MongoRepository<Retailer, String> {

	@Query(value="{ 'product.category' : ?0,'address.city' : ?1}")
	public List<Retailer> findRetailersByProductCategoryAndAddress(String category,String city);

	@Query(value="{ 'products.id' : ?0,'address.city' : ?1}")
	public List<Retailer> findRetailersByProductIdAndLocation(String productId,String city);



	@Query(value="{ 'id' : ?0}", fields="{ 'products' :1}")
	public List<Product> findProductById(String retailerId);
}
