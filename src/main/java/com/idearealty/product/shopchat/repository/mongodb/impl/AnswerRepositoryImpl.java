/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.CustomAnswerRepository;
import com.idearealty.product.shopchat.repository.mongodb.QuestionRepository;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;

/**
 * @author Debopam
 *
 */
public class AnswerRepositoryImpl implements CustomAnswerRepository {

	@Autowired
	QuestionRepository questionRepository;

	@Autowired
	RetailerRepository retailerRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public Answer submitAnswer(String questionid, String retailerid,String answerId,String answerText) {
		Question question = questionRepository.findOne(questionid);
		Retailer retailer = retailerRepository.findOne(retailerid);

		Answer answer = new Answer();
		answer.setId(answerId);
		answer.setQuestion(question);
		answer.setRetailer(retailer);
		answer.setAnswerText(answerText);

		Query query = new Query(Criteria.where("id").is(answerId));
		Update update = new Update().addToSet("answer", answerText).addToSet("retailer", retailer).addToSet("retailer", retailer);
		answer = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true).upsert(true), Answer.class);

		return answer;
	}

}
