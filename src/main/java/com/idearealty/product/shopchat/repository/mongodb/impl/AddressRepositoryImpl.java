/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.repository.mongodb.AddressRepository;

/**
 * @author Debopam
 *
 */
@Repository
public class AddressRepositoryImpl implements AddressRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	/**
	 * 
	 */
	public AddressRepositoryImpl() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.idearealty.product.shopchat.repository.mongodb.AddressRepository#findCities()
	 */
	@Override
	public List<String> findCities() {
		return mongoTemplate.getCollection("address").distinct("city");

	}

	@Override
	public Address saveOrUpdateAddress(Address address) {
		Query query = new Query(Criteria.where("city").is(address.getCity()).and("locality").is(address.getLocality()));
		Update update = new Update().set( "addrLine1", address.getAddrLine1())
				.set("addrLine2", address.getAddrLine2())
				.set("city", address.getCity())
				.set("locality", address.getLocality());
		Address addr = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true).upsert(true), Address.class,"address");
		return addr;
	}

	@Override
	public List<Address> findByLocality(String locality) {
		Query query = new Query(Criteria.where("locality").is(locality));

		List<Address> address = mongoTemplate.find(query, Address.class);
		return address;
	}

	@Override
	public List<String> findLocalities(String city) {
		Query query = new Query(Criteria.where("city").is(city));
		query.fields().include("locality");
		List<Address> addresses = mongoTemplate.find(query, Address.class);
		List<String> localities = new LinkedList<String>();
		for(Address address:addresses){
			localities.add(address.getLocality());
		}
		return localities;
	}

}
