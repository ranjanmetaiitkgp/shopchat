package com.idearealty.product.shopchat.repository.mongodb;

import com.idearealty.product.shopchat.persistence.model.Answer;

public interface CustomAnswerRepository {

	public Answer submitAnswer(String questionid, String retailerid,String answerId,String answer);


}
