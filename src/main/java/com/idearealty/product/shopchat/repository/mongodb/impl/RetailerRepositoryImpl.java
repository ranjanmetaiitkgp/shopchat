/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.AddressRepository;
import com.idearealty.product.shopchat.repository.mongodb.CustormRetailerRepositiry;
import com.idearealty.product.shopchat.repository.mongodb.ProductRepository;

/**
 * @author Debopam
 *
 */
public class RetailerRepositoryImpl implements CustormRetailerRepositiry {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	AddressRepository addressRepository;

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Retailer> findRetailersByProductNameAndCity(String productName,String city) {
		List<Address> address = addressRepository.findByLocality(city);

		List<Product> products = productRepository.findByProductName(productName);

		Query query = Query.query(Criteria.where("address").in(address).and("products").in(products));
		query.fields().include("user").include("address").include("shopName");

		List<Retailer> retailers = mongoTemplate.find(query, Retailer.class);

		System.out.println("Address count :"+address.size()+",Product Count :"+products.size()+",Retailer Count :"+retailers.size());
		System.out.println("Returne Retailers :"+retailers);
		return retailers;
	}

}
