/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.idearealty.product.shopchat.repository.mongodb.CustomProductRepository;

/**
 * @author Debopam
 *
 */
public class ProductRepositoryImpl implements CustomProductRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	/**
	 * 
	 */
	public ProductRepositoryImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<String> findDistinctCategory() {

		return mongoTemplate.getCollection("product").distinct("category");

	}


}
