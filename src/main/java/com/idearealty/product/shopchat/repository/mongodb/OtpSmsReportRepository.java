package com.idearealty.product.shopchat.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.idearealty.product.shopchat.persistence.model.OtpSmsReport;

public interface OtpSmsReportRepository extends MongoRepository<OtpSmsReport, String>{
	
	public OtpSmsReport findByPhoneNumberAndOtpCodeAndActiveIsTrue(String phoneNumber,String otpCode);
	
	public List<OtpSmsReport> findByPhoneNumberAndActiveIsTrue(String phoneNumber);
	
}
