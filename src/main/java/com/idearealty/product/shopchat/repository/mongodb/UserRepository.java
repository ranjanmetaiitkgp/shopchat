/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;

/**
 * @author Debopam
 *
 */
@Repository
public interface UserRepository extends MongoRepository<IdeaRealtyUser, String>{


	@Query(value="{'email' :?0}")
	public IdeaRealtyUser findByEmail(String email);

	@Query(value="{'username' :?0}")
	public IdeaRealtyUser findByUsername(String userName);



}
