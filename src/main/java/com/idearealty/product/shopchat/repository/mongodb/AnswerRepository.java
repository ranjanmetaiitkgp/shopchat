/**
 * 
 */
package com.idearealty.product.shopchat.repository.mongodb;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.product.shopchat.persistence.model.Answer;

/**
 * @author Debopam
 *
 */
@Repository
public interface AnswerRepository extends MongoRepository<Answer, String> {

	@Query(value="{'retailer.id' : ?0}")
	List<Answer> findByRetailerIDOrderByCreateDateDesc(String retailerID);

	@Query(value="{ 'lastModifiedDate' : { '$gte' : ?0},'createdBy':?1,'answerText' : {'$exists' : true, '$ne' : ''}}")
	public List<Answer> findAnswerByLastModifiedAndCreatedBy(Date createDate,String createdBy);

}
