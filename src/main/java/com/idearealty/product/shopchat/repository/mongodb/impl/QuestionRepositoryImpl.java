package com.idearealty.product.shopchat.repository.mongodb.impl;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.security.core.userdetails.UserDetails;

import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.LoginInfo;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.AnswerRepository;
import com.idearealty.product.shopchat.repository.mongodb.CustomQuestionRepository;
import com.idearealty.product.shopchat.repository.mongodb.LoginInfoRepository;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.idearealty.product.shopchat.repository.mongodb.UserRepository;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

public class QuestionRepositoryImpl implements CustomQuestionRepository {

	@Autowired
	LoginInfoRepository loginInfoRepository;

	@Autowired
	AnswerRepository answerRepository;

	@Autowired
	RetailerRepository retailerRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public Page<Question> findUsersQuestionsAnsweredSinceLastLogin(String userid,Pageable pageable) {

		Pageable loginInfoPageable = new PageRequest(0, 2);
		Page<LoginInfo> logingInfoPage = loginInfoRepository.findByUserIDOrderByCreateDateDesc(userid, loginInfoPageable);
		LoginInfo loginInfo = logingInfoPage.getContent() == null || logingInfoPage.getContent().isEmpty()?new LoginInfo()
		:logingInfoPage.getContent().size() ==2?logingInfoPage.getContent().get(1):logingInfoPage.getContent().get(0);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 01);
		cal.set(Calendar.MONDAY, Calendar.JANUARY);
		cal.set(Calendar.YEAR, 2015);

		List<Answer> answers = answerRepository.findAnswerByLastModifiedAndCreatedBy(
				logingInfoPage.getContent() == null || logingInfoPage.getContent().isEmpty()?cal.getTime():logingInfoPage.getContent().size() ==2?loginInfo.getCreateDate():cal.getTime(), userid);

		List<Question> questions = new LinkedList<>();
		int total = 0;
		for(Answer answer:answers){
			Query query = Query.query(Criteria.where("createdBy").is(userid).and("answers").in(answer));
			query.with(pageable);
			query.fields().exclude("answers").exclude("parentQuestion");

			List<Question> questionsForAnswer = mongoTemplate.find(query, Question.class);
			Question question = questionsForAnswer.get(0);
			question.addAnswer(answer);
			questions.add(question);
			total += mongoTemplate.count(query, Question.class);
		}

		Page<Question> questionsPage = new PageImpl<Question>(questions, pageable, total);
		return questionsPage;
	}


	@Override
	public Page<Answer> findQuestionsAwaitingRetailerAnswer(UserDetails userDetails, Pageable pageable) {
		Query retailerQuery = Query.query(Criteria.where("user").in(userRepository.findByUsername(userDetails.getUsername())));

		List<Retailer> retailers = mongoTemplate.find(retailerQuery, Retailer.class);

		Query query = Query.query(Criteria.where("retailer").in(retailers).and("answerText").is(""));
		query.with(pageable);
		query.fields().include("question").include("retailer").include("answerText");

		List<Answer> answers = mongoTemplate.find(query, Answer.class);

		for(Answer answer:answers){
			Query questionQuery = Query.query(Criteria.where("answers").in(answer));
			List<Question> questions = mongoTemplate.find(questionQuery, Question.class);
			if(null != questions && !questions.isEmpty())
				answer.setQuestion(questions.get(0));
		}

		long total = mongoTemplate.count(query, Answer.class);

		Page<Answer> answerPage = new PageImpl<Answer>(answers, pageable, /*(null == answers)?0:answers.size()*/total);
		return answerPage;
	}


	@Override
	public Page<Question> searchQuestionsByText(String[] text, Pageable pageable,String userid) {

		Query query = TextQuery.queryText(new TextCriteria().matchingAny(text)).sortByScore();
		query.with(pageable);
		if(null !=userid && !userid.isEmpty()){
			query.addCriteria(Criteria.where("createdBy").is(userid));
		}
		List<Question> questions = mongoTemplate.find(query, Question.class);
		long total = mongoTemplate.count(query, Question.class);
		Page<Question> questionPage = new PageImpl<Question>(questions, pageable, /*(null == answers)?0:answers.size()*/total);
		return questionPage;
	}

	@Override
	public Question getQuestionForAnswerId(String answerId) throws IdeaRealtyException{

		Query query = Query.query(Criteria.where("answerText").is("").and("_id").is(answerId));
		List<Answer> answers = mongoTemplate.find(query, Answer.class);

		if(null == answers || answers.isEmpty() || answers.size() > 1){
			throw new IdeaRealtyException("No Answer found to answer");
		}

		Query questionQuery = Query.query(Criteria.where("answers").in(answers));

		List<Question> questions = mongoTemplate.find(questionQuery, Question.class);

		if(null == questions || questions.isEmpty() || questions.size() > 1){
			throw new IdeaRealtyException("No Question found to answer");
		}

		return questions.get(0);
	}



}
