package com.idearealty.product.shopchat.util;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class MongoAuditAware implements AuditorAware<String> {

	@Override
	public String getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null || !authentication.isAuthenticated()) {
			//UserDetails user = new IdeaRealtyUser("UnAuntenticatedUser", "UnAuntenticatedUser", true, true, true, true, "DUMMY");
			return "UnAuntenticatedUser"; //user.getUsername();
		}

		if(authentication.getPrincipal() instanceof String){
			return (String)authentication.getPrincipal();
		}

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		//UserDetails user = new IdeaRealtyUser(userDetails.getUsername(), userDetails.getPassword(), userDetails.isEnabled(), true, true, true, userDetails.getAuthorities());
		return userDetails.getUsername();

	}

}
