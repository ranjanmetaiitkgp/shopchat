package com.idearealty.product.shopchat.web.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("delivery_report")
public class OtpDeliveryReportController {
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void process(@RequestBody Map<String, Object> payload) throws Exception {

		  System.out.println("++++++++++++  "+payload);

	}	

}
