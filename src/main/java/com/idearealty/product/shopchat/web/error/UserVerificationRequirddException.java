package com.idearealty.product.shopchat.web.error;

public final class UserVerificationRequirddException extends RuntimeException {

	private static final long serialVersionUID = 5861310537366287163L;

	public UserVerificationRequirddException() {
		super();
	}

	public UserVerificationRequirddException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public UserVerificationRequirddException(final String message) {
		super(message);
	}

	public UserVerificationRequirddException(final Throwable cause) {
		super(cause);
	}

}
