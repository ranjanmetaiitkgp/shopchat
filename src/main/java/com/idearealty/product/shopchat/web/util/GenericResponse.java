package com.idearealty.product.shopchat.web.util;

import java.io.IOException;
import java.util.List;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;




public class GenericResponse {
	private String message;
	private String error;

	public GenericResponse(String message) {
		super();
		this.message = message;
	}

	public GenericResponse(String message, String error) {
		super();
		this.message = message;
		this.error = error;
	}

	public GenericResponse(final List<FieldError> fieldErrors, final List<ObjectError> globalErrors) {
		super();
		final ObjectMapper mapper = new ObjectMapper();
		try {
			this.message = mapper.writeValueAsString(fieldErrors);
			this.error = mapper.writeValueAsString(globalErrors);
		} catch (final JsonProcessingException e) {
			this.message = "";
			this.error = "";
		} catch (IOException e) {
			this.message = "";
			this.error = "";
		}
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}



}
