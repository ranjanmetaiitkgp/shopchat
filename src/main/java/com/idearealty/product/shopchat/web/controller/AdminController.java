package com.idearealty.product.shopchat.web.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.service.QuestionsService;


@RestController
@RequestMapping(value="/admin", produces=MediaType.APPLICATION_JSON_VALUE,consumes={MediaType.APPLICATION_JSON_VALUE}, method = {RequestMethod.GET,RequestMethod.POST})
//@Secured({"ADMIN", "ROLE_ADMIN"})
public class AdminController {

	@Autowired
	QuestionsService questionsService;

	@Autowired
	private PagedResourcesAssembler<Question> questionAssembler;



	@RequestMapping(value ="/getAllQuestion",method = {RequestMethod.GET})
	public ResponseEntity<PagedResources<Resource<Question>>> getAllQuestions(@RequestBody String inputStr) throws JsonProcessingException, IOException {
		System.out.println("getAllQuestions called");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node;

		node = mapper.readTree(inputStr);
		String order = mapper.convertValue(node.get("order"), String.class);
		Integer pageNumber = mapper.convertValue(node.get("pageNumber"), Integer.class);
		Integer pageSize = mapper.convertValue(node.get("pageSize"), Integer.class);


		Sort sort = new Sort(null == order || "".equals(order.trim()) || "desc".equals(order)?Sort.Direction.DESC:Sort.Direction.DESC, "lastModifiedDate");
		PageRequest pageRequest = new PageRequest(null == pageNumber?0:pageNumber, null == pageSize?10:pageSize,sort);
		Page<Question> questions = questionsService.getQuestions(pageRequest);

		return new ResponseEntity<PagedResources<Resource<Question>>>(questionAssembler.toResource(questions), HttpStatus.OK);

	}


	@RequestMapping(value ="/searchQuestion",method = {RequestMethod.GET})
	public ResponseEntity<PagedResources<Resource<Question>>> searchQuestions(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String order = mapper.convertValue(node.get("order"), String.class);
		Integer pageNumber = mapper.convertValue(node.get("pageNumber"), Integer.class);
		Integer pageSize = mapper.convertValue(node.get("pageSize"), Integer.class);
		String searchText = mapper.convertValue(node.get("searchText"), String.class);

		Sort sort = new Sort(null == order || "".equals(order.trim()) || "desc".equals(order)?Sort.Direction.DESC:Sort.Direction.DESC, "lastModifiedDate");
		PageRequest pageRequest = new PageRequest(pageNumber, pageSize,sort);
		Page<Question> questions = questionsService.getQuestionsByQuestionText(searchText,pageRequest);

		return new ResponseEntity<PagedResources<Resource<Question>>>(questionAssembler.toResource(questions), HttpStatus.OK);
	}

	@RequestMapping(value ="/submitAnswer",method = {RequestMethod.POST})
	public ResponseEntity<Question> submitAnswer(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String questionid = mapper.convertValue(node.get("questionid"), String.class);
		String answerText = mapper.convertValue(node.get("answerText"), String.class);

		Question question = questionsService.addAnswer(questionid, answerText);
		return new ResponseEntity<Question>(question, HttpStatus.OK);
	}

	@RequestMapping(value ="/getQuestion",method = {RequestMethod.POST})
	public ResponseEntity<Question> getQuestion(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String questionid = mapper.convertValue(node.get("questionid"), String.class);

		Question question = questionsService.getQuestion(questionid);
		return new ResponseEntity<Question>(question, HttpStatus.OK);
	}

}
