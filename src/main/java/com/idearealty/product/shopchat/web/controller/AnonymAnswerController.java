package com.idearealty.product.shopchat.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.service.QuestionsService;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

@Controller
@RequestMapping("")
public class AnonymAnswerController {

	@Autowired
	QuestionsService questionsService;

	@RequestMapping(value ="/anonymqestionanswer",method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView anonymuousAnswer(HttpServletRequest request){
		System.out.println("anonymuousAnswer called :"+request.getAttribute("answerid"));

		ModelAndView anonymAnswerView = new ModelAndView("anonymqestionanswer");

		try {
			Question question = questionsService.getQuestionForAnswerId((String) request.getAttribute("answerid"));
			anonymAnswerView.addObject("questiontext", question.getQuestionText());
		} catch (IdeaRealtyException e) {
			e.printStackTrace();
		}

		anonymAnswerView.addObject("answerid", request.getAttribute("answerid"));
		return anonymAnswerView;
	}

	@RequestMapping(value ="/admininit",method = {RequestMethod.GET})
	public ModelAndView admininit(HttpServletRequest request){
		System.out.println("admin called ");

		ModelAndView adminview = new ModelAndView("admin");
		return adminview;
	}
	
	@RequestMapping(value ="/otpverification",method = {RequestMethod.GET})
	public ModelAndView otpverification(HttpServletRequest request){
		System.out.println("otpverification called ");

		ModelAndView otpverificationView = new ModelAndView("otpverification");
		return otpverificationView;
	}
	
}
