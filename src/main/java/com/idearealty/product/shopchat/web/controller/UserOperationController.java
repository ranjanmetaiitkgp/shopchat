package com.idearealty.product.shopchat.web.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idearealty.product.shopchat.event.OnQuestionSubmitEvent;
import com.idearealty.product.shopchat.persistence.model.Answer;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Question;
import com.idearealty.product.shopchat.service.AddressService;
import com.idearealty.product.shopchat.service.AnswerService;
import com.idearealty.product.shopchat.service.QuestionsService;
import com.idearealty.product.shopchat.service.UserService;
import com.idearealty.product.shopchat.web.error.IdeaRealtyException;

@RestController
@RequestMapping(value="/useroperation", produces=MediaType.APPLICATION_JSON_VALUE,consumes={MediaType.APPLICATION_JSON_VALUE,MediaType.TEXT_HTML_VALUE}
, method = {RequestMethod.GET,RequestMethod.POST})
public class UserOperationController {

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	UserService userService;

	@Autowired
	QuestionsService questionsService;

	@Autowired
	AddressService addressService;

	@Autowired
	AnswerService answerService;

	@Autowired
	private PagedResourcesAssembler<Question> questionAssembler;

	@Autowired
	private PagedResourcesAssembler<Answer> answerAssembler;

	@RequestMapping(value ="/submitQuestion", method = {RequestMethod.POST})
	public ResponseEntity<String> submitQuestion(@RequestBody String inputStr,HttpServletRequest httpServletRequest) throws Exception{
		try {
			IdeaRealtyUser activeUser = (IdeaRealtyUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(inputStr);
			String questionText = mapper.convertValue(node.get("question"), String.class);
			String retailerIds = mapper.convertValue(node.get("retailers"), String.class);
			String product = mapper.convertValue(node.get("product"), String.class);

			Question question = new Question();
			question.setQuestionText(questionText);

			questionsService.submitQustions(question, product, Arrays.asList(StringUtils.commaDelimitedListToStringArray(retailerIds)));
			final String appUrl = "http://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort() + httpServletRequest.getContextPath();
			eventPublisher.publishEvent(new OnQuestionSubmitEvent(activeUser, httpServletRequest.getLocale(), appUrl,question));

		} catch (IdeaRealtyException | IOException e) {
			e.printStackTrace();
			throw e;
		}
		return ResponseEntity.accepted().body("Question Submiteed successfully");
	}

	@RequestMapping(value ="/getAllQuestion",method = {RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<PagedResources<Resource<Question>>> getAllQuestions(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String userid = mapper.convertValue(node.get("userid"), String.class);
		String order = mapper.convertValue(node.get("order"), String.class);
		Integer pageNumber = mapper.convertValue(node.get("pageNumber"), Integer.class);
		Integer pageSize = mapper.convertValue(node.get("pageSize"), Integer.class);


		Sort sort = new Sort(null == order || "".equals(order.trim()) || "desc".equals(order)?Sort.Direction.DESC:Sort.Direction.DESC, "createDate");
		PageRequest pageRequest = new PageRequest(pageNumber, pageSize,sort);
		Page<Question> questions = questionsService.getQuestionsByUserId(userid, pageRequest);

		return new ResponseEntity<PagedResources<Resource<Question>>>(questionAssembler.toResource(questions), HttpStatus.OK);
	}

	@RequestMapping(value ="/getAllCities",method = {RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<String>> getAllCities(){
		List<String> citiList = addressService.getCities();
		return new ResponseEntity<List<String>>(citiList, HttpStatus.OK);
	}

	@RequestMapping(value ="/getLocality",method = {RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<String>> getLocality(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String city = mapper.convertValue(node.get("city"), String.class);

		List<String> localities = addressService.getLocalities(city);
		return new ResponseEntity<List<String>>(localities, HttpStatus.OK);
	}

	@RequestMapping(value ="/getUserDashBoardData",method = {RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<PagedResources<Resource<Question>>> getUserDashBoardData(@RequestBody String inputStr) throws JsonProcessingException, IOException{

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		Integer page = mapper.convertValue(node.get("page"), Integer.class);
		Integer size = mapper.convertValue(node.get("size"), Integer.class);

		if(null == page || page < 0) page = 0;
		if(null == size || size < 0) size = 5;

		PageRequest pageable = new PageRequest(page, size);

		return new ResponseEntity<PagedResources<Resource<Question>>>(questionAssembler.toResource(userService.userNotifications(pageable)), HttpStatus.OK);
	}

	//@Secured("RETAILER")
	@PreAuthorize("hasAnyRole('RETAILER','ADMIN','ROLE_ADMIN')")
	@RequestMapping(value ="/getRetailerDashBoardData",method = {RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<PagedResources<Resource<Answer>>> getRetailerDashBoardData(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		Integer page = mapper.convertValue(node.get("page"), Integer.class);
		Integer size = mapper.convertValue(node.get("size"), Integer.class);

		if(null == page || page < 0) page = 0;
		if(null == size || size < 0) size = 5;

		PageRequest pageable = new PageRequest(page, size);

		return new ResponseEntity<PagedResources<Resource<Answer>>>(answerAssembler.toResource(userService.retailerNotifcations(pageable)), HttpStatus.OK);
	}


	@RequestMapping(value ="/submitAnswer",method = {RequestMethod.POST})
	public ResponseEntity<Answer> submitAnswer(@RequestBody String inputStr,HttpServletRequest httpServletRequest) throws JsonProcessingException, IOException{

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String answerid = mapper.convertValue(node.get("answerid"), String.class);
		String answerText = mapper.convertValue(node.get("answer"), String.class);
		return new ResponseEntity<Answer>(answerService.submitAnswer(answerid, answerText), HttpStatus.OK);
	}

}
