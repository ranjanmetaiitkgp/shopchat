package com.idearealty.product.shopchat.web.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idearealty.product.shopchat.event.OnRegistrationCompleteEvent;
import com.idearealty.product.shopchat.persistence.model.Address;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.service.AddressService;
import com.idearealty.product.shopchat.service.IUserService;
import com.idearealty.product.shopchat.service.OtpSmsService;
import com.idearealty.product.shopchat.service.ProductService;
import com.idearealty.product.shopchat.service.RetailerService;
import com.idearealty.product.shopchat.validation.EmailExistsException;
import com.idearealty.product.shopchat.web.error.UserAlreadyExistException;
import com.idearealty.product.shopchat.web.error.UserNotFoundException;
import com.idearealty.product.shopchat.web.error.UserVerificationRequirddException;
import com.idearealty.product.shopchat.web.util.GenericResponse;

@RestController
@RequestMapping(value="/user", produces=MediaType.APPLICATION_JSON_VALUE,consumes={MediaType.APPLICATION_JSON_VALUE,MediaType.TEXT_HTML_VALUE})
public class RegistrationController {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	private IUserService userService;

	@Autowired
	private MessageSource messages;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	AddressService addressService;

	@Autowired
	ProductService productService;

	@Autowired
	RetailerService retailerService;

	@Autowired
	private Environment env;

	@Autowired
	private OtpSmsService mOtpSmsReportService;

	public RegistrationController() {

	}

	// Registration

	@RequestMapping(value = "/registration",method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<GenericResponse> registerUserAccount(@RequestBody String inputStr,final HttpServletRequest request) throws JsonProcessingException, IOException, EmailExistsException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String username = mapper.convertValue(node.get("username"), String.class);
		String email = mapper.convertValue(node.get("email"), String.class);
		String name = mapper.convertValue(node.get("name"), String.class);
		String isRetailer = mapper.convertValue(node.get("retailer"), String.class);
		String addressline1 = mapper.convertValue(node.get("addressline1"), String.class);
		String city = mapper.convertValue(node.get("city"), String.class);
		String state = mapper.convertValue(node.get("city"), String.class);
		String locality = mapper.convertValue(node.get("locality"), String.class);
		String shopname = mapper.convertValue(node.get("shopname"), String.class);
		String productIds = mapper.convertValue(node.get("productIds"), String.class);


		String role = "USER";
		if(null != isRetailer && ! isRetailer.isEmpty())
			role = role+",ROLE_RETAILER";
		IdeaRealtyUser accountDto = new IdeaRealtyUser(username,"",email,role);
		if(name.indexOf(" ") != -1){
			accountDto.setFirstName(name.split(" ")[0]);
			accountDto.setLastName(name.split(" ")[1]);
		}else{
			accountDto.setFirstName(name);
			accountDto.setLastName("");
		}
		LOGGER.debug("Registering user account with information: {}", accountDto,role);

		accountDto.setEnabled(false);
		System.out.println("User details :"+accountDto);
		final IdeaRealtyUser registered = createUserAccount(accountDto);

		if (registered == null) {
			IdeaRealtyUser existing = userService.findUserByUsername(username);
			if(null == existing){
				existing = userService.findUserByEmail(email);
			}
			if(!existing.isEnabled()){
				throw new UserVerificationRequirddException(username +" should be verified");
			}
			throw new UserAlreadyExistException();
		}

		final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

		eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));

		if(null != isRetailer && ! isRetailer.isEmpty()){
			Address address = new Address(addressline1, "", locality, city, state, "INDIA");
			address = addressService.saveOrUpdateAddress(address);

			Retailer retailer = new Retailer();
			retailer.setAddress(address);
			retailer.setUser(registered);
			retailer.setShopName(shopname);
			retailer.setProducts(productService.getProducts(productIds));
			retailerService.createReatiler(retailer);
		}

		return new ResponseEntity<GenericResponse>(new GenericResponse("success"), HttpStatus.OK);
	}

	@RequestMapping(value = "/registrationConfirm", method = {RequestMethod.POST})
	@ResponseBody
	public ResponseEntity<GenericResponse> confirmRegistration(@RequestBody String inputStr,final HttpServletRequest request) throws JsonProcessingException, IOException {


		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String phoneNumber = mapper.convertValue(node.get("phoneNumber"), String.class);
		String otp = mapper.convertValue(node.get("otp"), String.class);

		if(null != phoneNumber && null != otp){
			boolean verified = mOtpSmsReportService.verifyOtp(phoneNumber, otp);
			if(verified){
				IdeaRealtyUser user  = userService.findUserByUsername(phoneNumber);
				user.setEnabled(true);
				userService.saveRegisteredUser(user);
			}else{
				throw new UserVerificationRequirddException("Otp Doesn't match");
			}
		}


		//model.addAttribute("message", messages.getMessage("message.accountVerified", null, locale));
		return new ResponseEntity<GenericResponse>(new GenericResponse("verified"), HttpStatus.OK); //new ResponseEntity<String>("verified", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/regenerateOtp", method = {RequestMethod.POST})
	@ResponseBody
	public ResponseEntity<GenericResponse> regenerateOtp(@RequestBody String inputStr,final HttpServletRequest request) throws JsonProcessingException, IOException {


		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String phoneNumber = mapper.convertValue(node.get("phoneNumber"), String.class);

		if(null != phoneNumber){
				IdeaRealtyUser existing = userService.findUserByUsername(phoneNumber);
				if(null == existing){
					throw new UserNotFoundException();
				}
				
				mOtpSmsReportService.createSmsReportAndSendOtp(phoneNumber);
		}


		//model.addAttribute("message", messages.getMessage("message.accountVerified", null, locale));
		return new ResponseEntity<GenericResponse>(new GenericResponse("OTP_RE_GENERATED"), HttpStatus.OK); //new ResponseEntity<String>("verified", HttpStatus.OK);
	}

	/*
	// user activation - verification

	@RequestMapping(value = "/user/resendRegistrationToken", method = RequestMethod.GET,RequestMethod.POST)
	@ResponseBody
	public GenericResponse resendRegistrationToken(final HttpServletRequest request, @RequestParam("token") final String existingToken) {
		final VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
		final User user = userService.getUser(newToken.getToken());
		final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		final SimpleMailMessage email = constructResendVerificationTokenEmail(appUrl, request.getLocale(), newToken, user);
		mailSender.send(email);

		return new GenericResponse(messages.getMessage("message.resendToken", null, request.getLocale()));
	}

	// Reset password

	@RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
	@ResponseBody
	public GenericResponse resetPassword(final HttpServletRequest request, @RequestParam("email") final String userEmail) {
		final User user = userService.findUserByEmail(userEmail);
		if (user == null) {
			throw new UserNotFoundException();
		}

		final String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(user, token);
		final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		final SimpleMailMessage email = constructResetTokenEmail(appUrl, request.getLocale(), token, user);
		mailSender.send(email);
		return new GenericResponse(messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));
	}

	@RequestMapping(value = "/user/changePassword", method = RequestMethod.GET,RequestMethod.POST)
	public String showChangePasswordPage(final Locale locale, final Model model, @RequestParam("id") final long id, @RequestParam("token") final String token) {
		final PasswordResetToken passToken = userService.getPasswordResetToken(token);
		final User user = passToken.getUser();
		if (passToken == null || user.getId() != id) {
			final String message = messages.getMessage("auth.message.invalidToken", null, locale);
			model.addAttribute("message", message);
			return "redirect:/login.html?lang=" + locale.getLanguage();
		}

		final Calendar cal = Calendar.getInstance();
		if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
			return "redirect:/login.html?lang=" + locale.getLanguage();
		}

		final Authentication auth = new UsernamePasswordAuthenticationToken(user, null, userDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);

		return "redirect:/updatePassword.html?lang=" + locale.getLanguage();
	}

	@RequestMapping(value = "/user/savePassword", method = RequestMethod.POST)
	@PreAuthorize("hasRole('READ_PRIVILEGE')")
	@ResponseBody
	public GenericResponse savePassword(final Locale locale, @RequestParam("password") final String password) {
		final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userService.changeUserPassword(user, password);
		return new GenericResponse(messages.getMessage("message.resetPasswordSuc", null, locale));
	}

	// change user password

	@RequestMapping(value = "/user/updatePassword", method = RequestMethod.POST)
	@PreAuthorize("hasRole('READ_PRIVILEGE')")
	@ResponseBody
	public GenericResponse changeUserPassword(final Locale locale, @RequestParam("password") final String password, @RequestParam("oldpassword") final String oldPassword) {
		final User user = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		if (!userService.checkIfValidOldPassword(user, oldPassword)) {
			throw new InvalidOldPasswordException();
		}
		userService.changeUserPassword(user, password);
		return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
	}
	 */
	// NON-API
	/*
	private final SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final User user) {
		final String confirmationUrl = contextPath + "/regitrationConfirm.html?token=" + newToken.getToken();
		final String message = messages.getMessage("message.resendToken", null, locale);
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject("Resend Registration Token");
		email.setText(message + " \r\n" + confirmationUrl);
		email.setTo(((IdeaRealtyUser)user).getEmail());
		email.setFrom(env.getProperty("support.email"));
		return email;
	}*/

	private final SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final UserDetails user) {
		final String url = contextPath + "/user/changePassword?id=" + user.getUsername() + "&token=" + token;
		final String message = messages.getMessage("message.resetPassword", null, locale);
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(((IdeaRealtyUser)user).getEmail());
		email.setSubject("Reset Password");
		email.setText(message + " \r\n" + url);
		email.setFrom(env.getProperty("support.email"));
		return email;
	}

	private IdeaRealtyUser createUserAccount(final IdeaRealtyUser accountDto) {
		IdeaRealtyUser registered = null;
		try {
			registered = userService.registerNewUserAccount(accountDto);
		} catch (final EmailExistsException e) {
			return null;
		}
		return registered;
	}
}
