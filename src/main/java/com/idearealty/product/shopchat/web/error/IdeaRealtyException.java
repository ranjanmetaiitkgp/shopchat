package com.idearealty.product.shopchat.web.error;

public class IdeaRealtyException extends Exception {

	private static final long serialVersionUID = -4744430183523721711L;

	private String message = "IdeaRealty Excetion ..";

	public IdeaRealtyException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
