/**
 * 
 */
package com.idearealty.product.shopchat.web.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idearealty.product.shopchat.persistence.model.Product;
import com.idearealty.product.shopchat.persistence.model.Retailer;
import com.idearealty.product.shopchat.repository.mongodb.RetailerRepository;
import com.idearealty.product.shopchat.service.ProductService;
import com.idearealty.product.shopchat.service.RetailerService;

/**
 * @author Debopam
 *
 */
@RestController
@RequestMapping(value="/products", produces=MediaType.APPLICATION_JSON_VALUE,consumes={MediaType.APPLICATION_JSON_VALUE,MediaType.TEXT_HTML_VALUE})
public class ProductController {

	@Autowired
	ProductService productService;

	@Autowired
	RetailerService retailerService;

	@Autowired
	RetailerRepository retailerRepository;

	/**
	 * 
	 */
	public ProductController() {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value="/getCategories",method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<String>> getProductCategories(){
		List<String> productCategories = productService.getProductCategories();
		return new ResponseEntity<List<String>>(productCategories, HttpStatus.OK);
	}

	@RequestMapping(value="/getProducts",method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<Product>> getProductsByCategory(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String category = mapper.convertValue(node.get("category"), String.class);
		List<Product> prodcucts = productService.getProductsByCategory(category);
		return new ResponseEntity<List<Product>>(prodcucts, HttpStatus.OK);
	}

	@RequestMapping(value="/getRetailersByProductId",method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<Retailer>> getRetailersByProductIdAndLocation(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String productId = mapper.convertValue(node.get("productId"), String.class);
		String city = mapper.convertValue(node.get("city"), String.class);

		List<Retailer> retailers = retailerService.getRetailersByProductidAndLocation(productId, city);
		return new ResponseEntity<List<Retailer>>(retailers, HttpStatus.OK);
	}

	@RequestMapping(value="/getRetailersByProductName",method={RequestMethod.GET,RequestMethod.POST})
	public ResponseEntity<List<Retailer>> getRetailersByCategoryAndLocation(@RequestBody String inputStr) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(inputStr);
		String productName = mapper.convertValue(node.get("productName"), String.class);
		String locality = mapper.convertValue(node.get("locality"), String.class);
		List<Retailer> retailers = retailerService.getRetailersByProductNameAndLocation(productName, locality);
		return new ResponseEntity<List<Retailer>>(retailers, HttpStatus.OK);
	}

}
