package com.idearealty.product.shopchat.event;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.Question;



public class OnQuestionSubmitEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String appUrl;
	private final Locale locale;
	private final IdeaRealtyUser user;
	private final Question question;

	public OnQuestionSubmitEvent(IdeaRealtyUser user, Locale locale, String appUrl,Question question) {
		super(user);
		this.user = user;
		this.locale = locale;
		this.appUrl = appUrl;
		this.question = question;
	}

	/**
	 * @return the appUrl
	 */
	public String getAppUrl() {
		return appUrl;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @return the user
	 */
	public IdeaRealtyUser getUser() {
		return user;
	}

	/**
	 * @return the question
	 */
	public Question getQuestion() {
		return question;
	}




}
