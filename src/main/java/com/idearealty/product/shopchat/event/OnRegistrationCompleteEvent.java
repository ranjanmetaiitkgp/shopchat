package com.idearealty.product.shopchat.event;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;



public class OnRegistrationCompleteEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String appUrl;
	private final Locale locale;
	private final IdeaRealtyUser user;

	public OnRegistrationCompleteEvent(IdeaRealtyUser user, Locale locale, String appUrl) {
		super(user);
		this.user = user;
		this.locale = locale;
		this.appUrl = appUrl;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public Locale getLocale() {
		return locale;
	}

	public IdeaRealtyUser getUser() {
		return user;
	}
}
