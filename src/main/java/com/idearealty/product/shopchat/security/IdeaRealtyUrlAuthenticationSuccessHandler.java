package com.idearealty.product.shopchat.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.idearealty.product.shopchat.persistence.model.LoginInfo;
import com.idearealty.product.shopchat.repository.mongodb.LoginInfoRepository;

@Component("myAuthenticationSuccessHandler")
public class IdeaRealtyUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Autowired
	LoginInfoRepository loginInfoRepository;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		handle(request, response, authentication);
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.setMaxInactiveInterval(30*60);
		}


		clearAuthenticationAttributes(request);
	}

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		LoginInfo loginInfo = new LoginInfo(userDetails.getUsername(),request.getRemoteAddr(),request.getHeader("User-Agent"));
		//loginInfoRepository.insert(loginInfo);

		String targetUrl = determineTargetUrl(authentication);

		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}
		if(null != request.getSession(false)){
			request.setAttribute("sessid", request.getSession(false).getId());
			response.addCookie(new Cookie("sessidCookie", request.getSession(false).getId()));
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	protected String determineTargetUrl(Authentication authentication) {
		boolean isUser = false;
		boolean isAdmin = false;
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals("USER")
					|| grantedAuthority.getAuthority().equals("RETAILER")
					|| grantedAuthority.getAuthority().equals("ROLE_RETAILER")) {
				isUser = true;
			} else if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")
					|| grantedAuthority.getAuthority().equals("ADMIN")) {
				isAdmin = true;
				isUser = false;
				break;
			}
		}
		if (isUser) {
			return "/homepage.html?user=" + authentication.getName();
		} else if (isAdmin) {
			return "/homepage.html?user=" + authentication.getName(); //"/console.html";
		} else {
			throw new IllegalStateException();
		}
	}

	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}
}