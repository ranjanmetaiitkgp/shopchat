package com.idearealty.product.shopchat.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUser;
import com.idearealty.product.shopchat.persistence.model.IdeaRealtyUserDetails;
import com.idearealty.product.shopchat.repository.mongodb.UserRepository;
import com.idearealty.product.shopchat.web.error.UserNotFoundException;
import com.idearealty.product.shopchat.web.error.UserVerificationRequirddException;

@Service("userDetailsService")
//@Transactional
public class IdeaRealtyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MessageSource messages;


	@Autowired
	private LoginAttemptService loginAttemptService;

	@Autowired
	private HttpServletRequest request;

	public IdeaRealtyUserDetailsService() {
		super();
	}

	// API

	@Override
	public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException {
		String ip = request.getRemoteAddr();
		if (null != ip && loginAttemptService.isBlocked(ip)) {
			System.out.println("Login attempt from ip :"+ip);
			throw new RuntimeException("max tries reached blocked");
		}

		System.out.println("Trying to authenticate :"+userName);

		try {
			final IdeaRealtyUser user = userRepository.findByUsername(userName);

			if (user == null) {
				throw new UsernameNotFoundException(userName);
			}
			
			if(!user.isEnabled()){
				throw new UserVerificationRequirddException("Otp Verification required");
			}
			
			System.out.println("Authenticated User :"+user);
			//return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),user.isEnabled(),true, true, true,AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole()));
			return new IdeaRealtyUserDetails(user);
		} catch(UserVerificationRequirddException e){
			throw new UserVerificationRequirddException("OTP_VERIFICATION_REQ");
		}catch(UsernameNotFoundException e){
			throw new UserNotFoundException("UserNotFound");
		}catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}



	private final Collection<? extends GrantedAuthority> getGrantedAuthorities(final UserDetails user) {
		return user.getAuthorities();
	}

}
