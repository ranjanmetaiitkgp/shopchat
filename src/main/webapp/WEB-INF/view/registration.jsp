<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ page session="false"%>
<html>
<head>
<sec:csrfMetaTags />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<style>
.password-verdict {
	color: #000;
}
</style>
<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<title><spring:message code="label.form.title"></spring:message></title>
<sec:csrfMetaTags />
</head>
<body>
	<div class="container">
		<div>
			<h1>
				<spring:message code="label.form.title"></spring:message>
			</h1>
			<br>
			<form action="/" method="POST" enctype="utf8">
				<div class="form-group row">
					<label class="col-sm-3">
					<spring:message code="label.user.mobilenumber"></spring:message></label> 
					<span class="col-sm-5">
						<input class="form-control" type="text" name="username" id="username" placeholder="Mobile Number" required /></span> 
						<span id="username" class="alert alert-danger col-sm-4" style="display: none"></span>

				</div>
				<div class="form-group row">
					<label class="col-sm-3">
					<spring:message code="label.user.firstName"></spring:message></label> 
					<span class="col-sm-5">
						<input class="form-control" type="text" name="firstName" id="firstName" placeholder="First Name" required /></span> 
						<span id="firstNameError" class="alert alert-danger col-sm-4" style="display: none"></span>

				</div>
				<div class="form-group row">
					<label class="col-sm-3"><spring:message
							code="label.user.lastName"></spring:message></label> <span
						class="col-sm-5"><input class="form-control" type="text" placeholder="Last Name" id="lastname"
						name="lastName" required /></span> <span id="lastNameError"
						class="alert alert-danger col-sm-4" style="display: none"></span>

				</div>
				<div class="form-group row">
					<label class="col-sm-3"><spring:message
							code="label.user.email"></spring:message></label> <span class="col-sm-5"><input
						type="email" class="form-control" name="email" id="email" required /></span>
					<span id="emailError" class="alert alert-danger col-sm-4"
						style="display: none"></span>

				</div>
				
				<br>
				<button type="submit" class="btn btn-primary">
					<spring:message code="label.form.submit"></spring:message>
				</button>
				<sec:csrfInput />
			</form>
			<br> <a href="<c:url value="login.html" />"><spring:message
					code="label.form.loginLink"></spring:message></a>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
	
$(document).ready(function () {
	
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	$('form').submit(function(event) {
		register(event);
	});
	/*
	$(":password").keyup(function(){
		if($("#password").val() != $("#matchPassword").val()){
	        $("#globalError").show().html('<spring:message code="PasswordMatches.user"></spring:message>');
	    }else{
	    	$("#globalError").html("").hide();
	    }
	});
	
	options = {
		    common: {minChar:8},
		    ui: {
		    	showVerdictsInsideProgressBar:true,
		    	showErrors:true,
		    	errorMessages:{
		    		  wordLength: '<spring:message code="error.wordLength"/>',
		    		  wordNotEmail: '<spring:message code="error.wordNotEmail"/>',
		    		  wordSequences: '<spring:message code="error.wordSequences"/>',
		    		  wordLowercase: '<spring:message code="error.wordLowercase"/>',
		    		  wordUppercase: '<spring:message code="error.wordUppercase"/>',
		    	      wordOneNumber: '<spring:message code="error.wordOneNumber"/>',
		    		  wordOneSpecialChar: '<spring:message code="error.wordOneSpecialChar"/>'
		    		}
		    	}
		};
	 $('#password').pwstrength(options);
	 */
});


function register(event){
	event.preventDefault();
    $(".alert").html("").hide();
    $(".error-list").html("");


    $(".alert").html("").hide();
    $(".error-list").html("");
    

    
    var data= {
        	username:$('#username').val(),
        	name:$('#firstName').val()+' '+$('#lastname').val(),
        	email:$('#email').val()
        };
    
    $.ajax({
        url: '<c:url value="/user/registration.json"/>',
        dataType: "json",
        type: "POST",
        data: JSON.stringify(data),
        beforeSend: function(xhr) {
        	alert('data :'+JSON.stringify(data));
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
			},
        success: function(data, textStatus, jqXHR)
        {
        	if(data.message == "success"){
                window.location.href = "<c:url value="/otpverification.html"></c:url>";
                
             }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	alert("/registration :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
        	console.log("/registration :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
        }
    });
    
    

  
}
/*
function register(event){
	event.preventDefault();
    $(".alert").html("").hide();
    $(".error-list").html("");
    if($("#password").val() != $("#matchPassword").val()){
    	$("#globalError").show().html('<spring:message code="PasswordMatches.user"></spring:message>');
    	return;
    }
    var formData= $('form').serialize();
    $.post("<c:url value="/user/registration"/>",formData ,function(data){
        if(data.message == "success"){
            window.location.href = "<c:url value="/successRegister.html"></c:url>";
        }
        
    })
    .fail(function(data) {
        if(data.responseJSON.error.indexOf("MailError") > -1)
        {
            window.location.href = "<c:url value="/emailError.html"></c:url>";
        }
        else if(data.responseJSON.error == "UserAlreadyExist"){
            $("#emailError").show().html(data.responseJSON.message);
        }
        else if(data.responseJSON.error.indexOf("InternalError") > -1){
            window.location.href = "<c:url value="/login.html"></c:url>" + "?message=" + data.responseJSON.message;
        }
        else{
        	var errors = $.parseJSON(data.responseJSON.message);
            $.each( errors, function( index,item ){
                $("#"+item.field+"Error").show().html(item.defaultMessage);
            });
            errors = $.parseJSON(data.responseJSON.error);
            $.each( errors, function( index,item ){
                $("#globalError").show().append(item.defaultMessage+"<br>");
            });
        }
    });
}*/
  
</script>
</body>

</html>