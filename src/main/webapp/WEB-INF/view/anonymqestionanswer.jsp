<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
    uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/css/homepage.css">
<link rel="stylesheet" href="resources/css/bootstrap-multiselect.css" type="text/css"/>
<style type="text/css">
/* make sidebar nav vertical */ 
@media (min-width: 768px) {
  .sidebar-nav .navbar .navbar-collapse {
    padding: 0;
    max-height: none;
  }
  .sidebar-nav .navbar ul {
    float: none;
  }
  .sidebar-nav .navbar ul:not {
    display: block;
  }
  .sidebar-nav .navbar li {
    float: none;
    display: block;
  }
  .sidebar-nav .navbar li a {
    padding-top: 12px;
    padding-bottom: 12px;
  }
}
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<title><spring:message code="label.pages.home.title"></spring:message></title>
</head>
<body>

	<div id="page-wrapper" class="container-fluid">

		<div id="content-wrapper">
			<div class="">
				<div class=row>
					 <div class="col-md-8 col-xs-8">
					 	<div class="panel panel-default">
					 	  <div class="panel-heading"><c:out value = "${param.questiontext}" /></div>
						  <div class="panel-body">
						    <input id="answertext" type="text" class="form-control" placeholder="Type your message here..." />
						  </div>
						  <div class="panel-footer">
						  <input type="hidden" id="answerid" value='<c:out value = "${param.answerid}" />'/>
						  <span class="input-group-btn"><button class="btn btn-warning btn-md pull-right" onclick="submitAnswer()">Send</button></span>
						  </div>
						</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.cookie.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-multiselect.js"/>"></script>
	
	<script type="text/javascript">
	
	
	
	function submitAnswer(id){
		
		var answerdata = {
            	answerid : $('#answerid').val(),
            	answer :$('#answertext').val()
            };
		
		$.ajax({
            url: 'useroperation/submitAnswer.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(answerdata) ,
            beforeSend: function(xhr) {
            	
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	$('#panelid'+id).empty();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("submitAnswer :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log( "submitAnswer :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
	}
	
	$(document).ready(function() {
		
		
		
		
		
		
		
		
		
		//
	});
	</script>
</body>
</html>