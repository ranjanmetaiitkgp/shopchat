<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="messages" />
<%@ page session="true"%>
<fmt:message key="message.password" var="noPass" />
<fmt:message key="message.username" var="noUser" />

<html>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/css/styles.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<title><spring:message code="label.pages.home.title"></spring:message></title>

<style type="text/css">
.wrapper{width:500px;margin-left:auto;margin-right:auto}
label{padding-left:0 !important}
</style>
</head>
<body>
<c:if test="${param.message != null}">
<div class="alert alert-info">
${param.message}
</div>
</c:if>

    <div class="text-center" style="padding:50px 0">
	<div class="logo">OTP Verification</div>
	<!-- Main Form -->
	<c:url value="/user/regitrationConfirm" var="loginurl"/>
	<div class="login-form">
		<form id="login-form" class="text-left" action="${loginurl}" method='POST'>
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="username" class="col-sm-3 sr-only">Username</label>
						<input type="text" class="form-control" id="username" name="username" placeholder="username">
						
						<input type="text" class="form-control" id="otp" name="otp" placeholder="otp" value="" >
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			
		</form>
	</div>
	<!-- end:Main Form -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<script type="text/javascript">

(function($) {
    "use strict";
	
	// Options for Message
	//----------------------------------------------
  var options = {
	  'btn-loading': '<i class="fa fa-spinner fa-pulse"></i>',
	  'btn-success': '<i class="fa fa-check"></i>',
	  'btn-error': '<i class="fa fa-remove"></i>',
	  'msg-success': 'All Good! Redirecting...',
	  'msg-error': 'Wrong login credentials!',
  };

	// Login Form
	//----------------------------------------------
	// Validation
  $("#login-form").validate({
  	rules: {
      username: "required",
  	  password: "required",
    },
  	errorClass: "form-invalid"
  });
  
	// Form Submission
  $("#login-form").submit(function() {
  	remove_loading($(this));
  	var data= {
  			phoneNumber:$('#username').val(),
        	otp:$('#otp').val()
        };
    
    $.ajax({
        url: '<c:url value="/user/registrationConfirm.json"/>',
        dataType: "json",
        type: "POST",
        data: JSON.stringify(data),
        beforeSend: function(xhr) {
        	alert('data :'+JSON.stringify(data));
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
			},
        success: function(data, textStatus, jqXHR)
        {
        	if(data.message == "verified"){
                //window.location.href = "<c:url value="/login.html"></c:url>";
                alert('verification successful');
             }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	alert("/regitrationConfirm :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
        	console.log("/regitrationConfirm :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
        }
    });
    
	
  });
	
	
	// Loading
	//----------------------------------------------
  function remove_loading($form)
  {
  	$form.find('[type=submit]').removeClass('error success');
  	$form.find('.login-form-main-message').removeClass('show error success').html('');
  }

  function form_loading($form)
  {
    $form.find('[type=submit]').addClass('clicked').html(options['btn-loading']);
  }
  
  function form_success($form)
  {
	  $form.find('[type=submit]').addClass('success').html(options['btn-success']);
	  $form.find('.login-form-main-message').addClass('show success').html(options['msg-success']);
  }

  function form_failed($form)
  {
  	$form.find('[type=submit]').addClass('error').html(options['btn-error']);
  	$form.find('.login-form-main-message').addClass('show error').html(options['msg-error']);
  }
	
})(jQuery);

</script>

</body>

</html>