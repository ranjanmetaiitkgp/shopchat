<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
    uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/css/homepage.css">
<link rel="stylesheet" href="/resources/css/bootstrap-multiselect.css" type="text/css"/>
<style type="text/css">
/* make sidebar nav vertical */ 
@media (min-width: 768px) {
  .sidebar-nav .navbar .navbar-collapse {
    padding: 0;
    max-height: none;
  }
  .sidebar-nav .navbar ul {
    float: none;
  }
  .sidebar-nav .navbar ul:not {
    display: block;
  }
  .sidebar-nav .navbar li {
    float: none;
    display: block;
  }
  .sidebar-nav .navbar li a {
    padding-top: 12px;
    padding-bottom: 12px;
  }
}
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<title><spring:message code="label.pages.home.title"></spring:message></title>
</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand"href="<c:url value="/homepage.html" />"><spring:message code="label.pages.home.title"></spring:message></a>
	    </div>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<c:url value="/logout" />"><spring:message code="label.pages.logout"></spring:message></a> </li>
	      </ul>
	    </div>
	</nav>
	
	
	<div id="page-wrapper" class="container-fluid">
		<!-- Sidebar -->
		<div class="col-md-3 col-xs-3">
		    <div class="sidebar-nav">
		      <div class="navbar navbar-default" role="navigation">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <span class="visible-xs navbar-brand">Shop Chat</span>
		        </div>
		        <div class="navbar-collapse collapse sidebar-navbar-collapse">
		          <ul class="nav navbar-nav">
		            <li class="active"><a href="#">Quick Navigation</a></li>
		            <li><a href="#">My Questions</a></li>
		            <li><a href="#">DashBoard</a></li>
		            <sec:authorize  access="hasRole('ADMIN')">
					    <li><a href="/admininit">Admin Functioanlities</a></li>
					</sec:authorize>
					<sec:authorize  access="hasRole('ROLE_ADMIN')">
					    <li><a href="/admininit">Admin Role Admin Functioanlities</a></li>
					</sec:authorize>
					<sec:authorize  url="/admin">
					    <li><a href="/admininit">Admin URL Functioanlities</a></li>
					</sec:authorize>
		            <sec:authentication property="authorities" var="roles" scope="page" />
						    <c:forEach var="role" items="${roles}">
						    <li>${role}</li>
						    </c:forEach>
						
		          </ul>
		        </div><!--/.nav-collapse -->
		      </div>
		    </div>
		  </div>
		
		<!-- End Sidebar -->
		<div id="content-wrapper">
			<div class="">
				<div class=row>
					 <div class="col-md-4 col-xs-4">
					 	<div class="well"><span>User Inbox Messages</span>
					 		<div class="panel-group" id="userquestions" role="tablist" aria-multiselectable="true">
							  
							</div>
					 	</div>
					 	<div class="well"><span>Retailer Inbox Messages</span>
					 		<div class="panel-group" id="retaileranswers" role="tablist" aria-multiselectable="true">
							  
							</div>
					 	</div>
					 </div>
					 <div class="col-md-4 col-xs-4">
					 		<div class="panel panel-default">
						        <div class="panel-heading">
						          <div class="panel-title">
						            <i class="glyphicon glyphicon-wrench pull-right"></i>
						            <h4>Submit New Question</h4>
						          </div>
						        </div>
						        <div class="panel-body">
						          
						          <form class="form form-vertical">
						            <div class="control-group">
						              <label>City</label>
						              <div class="controls">
						                <select id="city" class="form-control">
	      								</select>
						              </div>
						            </div>
						            <div class="control-group">
						              <label>Locality</label>
						              <div class="controls">
						                <select id="locality" class="form-control">
	      								</select>
						              </div>
						            </div>
						            <div class="control-group">
						              <label>Product Category</label>
						              <div class="controls">
						                <select id="productcategorries" class="form-control">
	      								</select>
						              </div>
						            </div> 
						            <div class="control-group">
						              <label>Products</label>
						              <div class="controls">
						                <select id="products" class="form-control">
	      								</select>
						              </div>
						            </div>      
						            <div class="control-group">
						              <label>Retailers</label>
						              <div class="controls">
						                <select id="retailers" class="form-control col-sm-10" multiple="multiple">
	      								</select>
						              </div>
						            </div>   
						            <div class="control-group">
						              <label>Details</label>
						              <div class="controls">
						                <textarea class="form-control" id="questiontext"></textarea>
						              </div>
						            </div> 
						                
						            <div class="control-group">
						              <label></label>
						              <div class="controls">
						                <button type="button" class="btn btn-primary" id="postQuestion">
						                  Post Question
						                </button>
						              </div>
						            </div>   
						            
						          </form>
						          
						          
						        </div><!--/panel content-->
						      </div>
					 </div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.cookie.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-multiselect.js"/>"></script>
	
	<script type="text/javascript">
	
	function getLocality(){
		var data = {
				city:$('#city option:selected').val()	
		};
		$('#locality').empty();
		$.ajax({
            url: 'useroperation/getLocality.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(xhr) {
            	alert("selected city :"+$('#city option:selected').val());
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	
            	console.log( "/getLocality :"+JSON.stringify(data));
                //data - response from server
				$("#locality").append('<option value="">Select Locality</option>');
            	$.each(data,function(i,data){
      			   $("#locality").append('<option value="'+data+'">'+data+'</option>');
      			});
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("locality :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log("locality :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
	}
	
	function getProducts(){
		$('#products').empty();
		var data= {
        	category:$('#productcategorries option:selected').val()
        };
		$.ajax({
            url: 'products/getProducts.json',
            dataType: "json",
            type: "POST",
            
            data: JSON.stringify(data),
            beforeSend: function(xhr) {
            	//alert('data :'+$('#productcategorries option:selected').val());
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//alert("/getProducts :"+JSON.stringify(data));
            	console.log( "/getProducts :"+JSON.stringify(data));
                //data - response from server
				$("#products").append('<option value="">Select Products</option>');
            	$.each(data,function(i,data){
      			   $("#products").append('<option value="'+data.productName+'">'+data.productName+'</option>');
      			});
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("/getProducts :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log("/getProducts :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
	}
	
	function getRetailers(){
		
		$('#retailers').empty();
		var data= {
			locality:$('#locality option:selected').val(),
        	productName:  $('#products option:selected').val()
        };
        
		var retailerdata = [];
		
		$.ajax({
            url: 'products/getRetailersByProductName.json',
            dataType: "json",
            type: "POST",
            
            data: JSON.stringify(data),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//alert("/getRetailersByProductName :"+JSON.stringify(data));
            	console.log( "/getRetailersByProductName :"+JSON.stringify(data));
                //data - response from server
            	
            	$.each(data,function(i,data){
       			   //$("#retailers").append('<option value="'+data.id+'">'+data.user.firstName+' '+data.user.lastName+'</option>');
            		var optiondata = {
                            label: data.shopName+' Location ('+data.address.locality +','+data.address.city+')', // +'<div class="row"><div class="well well-sm">'+data.address.locality +','+data.address.city +'</div></div>',
                            value: data.id
                    };
            		retailerdata.push(optiondata);
       			});
       			
            	$('#retailers').multiselect('dataprovider', retailerdata);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("/getRetailersByProductName :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log("/getRetailersByProductName :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
	}
	
	function postQuestion(){
		
		var options = '';
        $('#retailers option:selected').each(function() {
            options += $(this).val() + ',';
        });
		console.log('Retailers :'+options);
       // var questionInputText = $('textarea#questiontext').val();
        //var selectedretailers = options.substr(0, options.length - 1);
        //var productName = $('#products option:selected').val();
        
        var questiondata = {
        		question: $('textarea#questiontext').val(),
        		retailers : options.substr(0, options.length - 1),
        		product: $('#products option:selected').val()
        };
        	
        	
		$.ajax({
            url: 'useroperation/submitQuestion.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(questiondata),
            beforeSend: function(xhr) {
            	//alert(JSON.stringify(questiondata));
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	alert('Question submitted successfully');
            	//alert("/getRetailersByProductName :"+JSON.stringify(data));
            	console.log( "postQuestion :"+JSON.stringify(data));
                //data - response from server
            	
            	
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	
            	console.log("/postQuestion :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            }
        });
		
	}
	
	function submitAnswer(id){
		var originalAnswerID = 'answerid'+id;
		var answertext = 'answertext'+id;
		
		var answerdata = {
            	answerid : $('#'+originalAnswerID).val(),
            	answer :$('#'+answertext).val()
            };
		
		$.ajax({
            url: 'useroperation/submitAnswer.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(answerdata) ,
            beforeSend: function(xhr) {
            	
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	$('#panelid'+id).empty();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("submitAnswer :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log( "submitAnswer :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
	}
	
	$(document).ready(function() {
		
		$('#city').change(function(){		
			getLocality();
	    });
		
		$('#productcategorries').change(function(){		
			getProducts();
	    });
		
		
		
		$('#retailers').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
		
		$('#locality').change(function(){	
			var productName = $('#products option:selected').val();
        	var locality = $('#locality').val();
        	if(productName.length != 0  && locality.length != 0){
			 	getRetailers();
        	}
	    });
		
		$('#products').change(function(){
			var productName = $('#products option:selected').val();
        	var locality = $('#locality').val();
        	if(productName.length != 0  && locality.length != 0){
			 	getRetailers();
        	}
	    });
		
		$('#postQuestion').click(function(){
			//alert('Post Question click');
			postQuestion();
	    });
		
		$.ajax({
            url: 'useroperation/getAllCities.json',
            dataType: "json",
            type: "POST",
            data: {
                
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//alert("getcities :"+JSON.stringify(data));
            	console.log( "getcities :"+JSON.stringify(data));
                //data - response from server
                $("#city").append('<option value="">Select City</option>');
            	$.each(data,function(i,data){
     			   $("#city").append('<option value="'+data+'">'+data+'</option>');
     			});
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("getcities :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log( "getcities :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
		
		$.ajax({
            url: 'products/getCategories.json',
            dataType: "json",
            type: "POST",
            data: {
                
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//alert("/getCategories :"+JSON.stringify(data));
            	console.log( "/getCategories :"+JSON.stringify(data));
            	
            	$("#productcategorries").append('<option value="">Select Product Categories</option>');
            	
            	$.each(data,function(i,data){
      			   $("#productcategorries").append('<option value="'+data+'">'+data+'</option>');
      			});
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("/getCategories :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log("/getCategories :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
		
		
		var dashBoarddata= {
        	"page":0,
        	"size":10
        };
		
		$.ajax({
            url: 'useroperation/getUserDashBoardData.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(dashBoarddata) ,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//alert("getUserDashBoardData :"+JSON.stringify(data));
            	console.log( "getUserDashBoardData :"+JSON.stringify(data));
                //data - response from server
                if(data.content.length != 0){
	            	$.each(data.content,function(i,data){
	            		console.log( "getUserDashBoardData each:"+JSON.stringify(data));
	            		var userquestions = '<div class="panel panel-default" id="panelid'+i+'">'
	            			  +'<div class="panel-heading" role="tab" id="headingOne">'
	            			  +'<h4 class="panel-title">'
	            			  +'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">'
	            			  +data.product.productName
	            			  +'</a>'
	            			  +'</h4>'
	            			  +'</div>'
	            			  +'<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">'
	            			  +'<div class="panel-heading"><h3 class="panel-title"> Question:<strong>'+data.questionText+'</strong></h3></div>'
	            			  +' <div class="list-group">';
	            			  $.each(data.answers, function (j, answer) {
	            				  userquestions +='<div class="list-group-item"><div class="list-group-item-heading">'+answer.answerText +'</div><p class="list-group-item-text list-group-item-info text-right"> Retailer :'+ answer.retailer.shopName +'</p></div>';
	            				  //userquestions +=;
	            	            });
	            			  userquestions +='</div>';           			  
	            			  userquestions +='</div></div>';
	            		
	            			  
	            		$("#userquestions").append(userquestions);
	      			   
	      			});
              }
               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("getUserDashBoardData Error:"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            	console.log("getUserDashBoardData Error:"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            }
        });
		
		
		
		$.ajax({
            url: 'useroperation/getRetailerDashBoardData.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(dashBoarddata) ,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	if(data.length != 0){
	            	//alert("getRetailerDashBoardData :"+JSON.stringify(data.content));
	            	//
	            	//
	                //data - response from server
	                
	                
	                if(data.content.length != 0){
	                	console.log( "getRetailerDashBoardData data.content:"+JSON.stringify(data.content[0]));
	            	   $.each(data.content,function(i,data){
	            		console.log( "getRetailerDashBoardData each:"+JSON.stringify(data));
	            		if(data.question.length != 0){
	            			//console.log( "getRetailerDashBoardData data.content.question:"+JSON.stringify(data.question));
		            		var reatileranswerpannel = '<div class="panel panel-default" id="panelid'+i+'">'
		            			  +'<div class="panel-heading" role="tab" id="headingOne">'
		            			  +'<h4 class="panel-title">'
		            			  +'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">'
		            			  +data.question.product.productName
		            			  +'</a>'
		            			  +'</h4>'
		            			  +'</div>'
		            			  +'<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">'
		            			  +' <div class="panel-body">'
		            			  +data.question.questionText
		            			  +'</div>'
		            			  +'<div class="panel-footer">'
		            			  +'<div class="input-group">'
		            			  +'<input id="answertext'+i+'" type="text" class="form-control input-sm" placeholder="Type your message here..." />'
		            			  +'<input type="hidden" id="answerid'+i+'" value="'+data.question.answers[0].id + '"/>'
		            			  +'<span class="input-group-btn">'
		            			  +'<button class="btn btn-warning btn-sm" id="'+i+'" onclick="submitAnswer('+i+')">'
		            			  +'Send</button>'
		            			  +'</span>'
		            			  +'</div>'
		            			  +'</div>'
		            			  +'</div>'
		            			  +'</div>';
		            		
		            			  
		            		$("#retaileranswers").append(reatileranswerpannel);
	            		//counter++;
	            		}
	      			});
            	}
               }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	alert("getRetailerDashBoardData Error:"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            	console.log("getRetailerDashBoardData Error:"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            }
        });
		
		//
	});
	</script>
</body>
</html>