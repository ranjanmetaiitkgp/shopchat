<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/css/homepage.css">
<link rel="stylesheet" href="/resources/css/bootstrap-multiselect.css" type="text/css"/>
<style type="text/css">
/* make sidebar nav vertical */ 
@media (min-width: 768px) {
  .sidebar-nav .navbar .navbar-collapse {
    padding: 0;
    max-height: none;
  }
  .sidebar-nav .navbar ul {
    float: none;
  }
  .sidebar-nav .navbar ul:not {
    display: block;
  }
  .sidebar-nav .navbar li {
    float: none;
    display: block;
  }
  .sidebar-nav .navbar li a {
    padding-top: 12px;
    padding-bottom: 12px;
  }
}
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<title><spring:message code="label.pages.home.title"></spring:message></title>
</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand"href="<c:url value="/homepage.html" />"><spring:message code="label.pages.home.title"></spring:message></a>
	    </div>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<c:url value="/logout" />"><spring:message code="label.pages.logout"></spring:message></a> </li>
	      </ul>
	    </div>
	</nav>
	
	
	<div id="page-wrapper" class="container-fluid">
		<!-- Sidebar -->
		<div class="col-md-3 col-xs-3">
		    <div class="sidebar-nav">
		      <div class="navbar navbar-default" role="navigation">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <span class="visible-xs navbar-brand">Shop Chat</span>
		        </div>
		        <div class="navbar-collapse collapse sidebar-navbar-collapse">
		          <ul class="nav navbar-nav">
		            <li class="active"><a href="#">Quick Navigation</a></li>
		            <li><a href="#">My Questions</a></li>
		            <li><a href="#">DashBoard</a></li>
		            <sec:authorize access="hasRole('admin')">
					    <li><a href="/admin">Admin Functioanlities</a></li>
					</sec:authorize>
		            
		          </ul>
		        </div><!--/.nav-collapse -->
		      </div>
		    </div>
		  </div>
		
		<!-- End Sidebar -->
		<div id="content-wrapper">
			<div class="">
				<div class=row>
					 <div class="col-md-4 col-xs-4">
					 	<div class="list-group" id="qsset">
						  
						</div>
					 </div>
					 <div class="col-md-5 col-xs-5">
					 		<div class="panel panel-default">
							  <div class="panel-body">
							    <dl class="dl-horizontal" id="qsdetail">
								</dl>
							  </div>
							</div>
					 </div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.cookie.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-multiselect.js"/>"></script>
	
	<script type="text/javascript">
	
	
	function getQuestionDetail(id){

		alert("getQuestionDetail :"+id);
		var qsdata = {
				"questionid" : id
            };
		$('#qsdetail').empty();
		
		$.ajax({
            url: '/admin/getQuestion.json',
            dataType: "json",
            type: "POST",
            data: JSON.stringify(qsdata) ,
            beforeSend: function(xhr) {
            	alert(JSON.stringify(qsdata));
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	console.log( "getQuestion:"+JSON.stringify(data));
            	var qsdetails ='<dt>Question Asked</dt><dd>'+data.questionText+'</dd>';
            	qsdetails+='<dt>Product Category</dt><dd>'+data.product.category+'</dd>';
            	qsdetails+='<dt>Product Name</dt><dd>'+data.product.productName+'</dd>';
            	//qsdetails+='<dt>Question Asked</dt><dd>'+data.questionText+'</dd>';
            	//if(data.answers.length != 0){
	            	qsdetails+='<dt>Answers</dt>';
	            	qsdetails+='<dl class="dl-horizontal">';
	            	$.each(data.answers,function(i,data){ 
	            		qsdetails+='<dt>Retailer</dt><dd><address>'
	            		  +'<strong>'+data.retailer.shopName+'</strong><br>'
	            		  +data.retailer.address.addrLine1+'<br>'
	            		  +data.retailer.address.addrLine2+'<br>'
	            		  +data.retailer.address.locality+'<br>'
	            		  +data.retailer.address.city+'<br>'
	            		  +'<abbr title="Phone">P:</abbr>'+ data.retailer.user.username
	            		  +'</address></dd>';
	            		qsdetails+='<dt>Answer</dt><dd>'+data.answerText+'</dd>';
	      			});
	            	qsdetails+='</dl>';
            	//} 
            	$('#qsdetail').append(qsdetails);
            	
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
            	//var err = eval("(" + jqXHR.responseText + ")");
            	  //alert(err.Message);
            	alert("getQuestionDetail :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            	console.log( "getQuestionDetail :"+jqXHR.responseText+":"+textStatus+":"+errorThrown);
            }
        });
	} 
	
	
	$(document).ready(function() {
		
		var questionPageData= {
	        	"page":0,
	        	"size":10
	     };
		
		$.ajax({
			url: 'admin/getAllQuestion.json',
            dataType: "json",
            type: "POST",
            data: '{}',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");

   			},
            success: function(data, textStatus, jqXHR)
            {
            	//if(data.length != 0){
	                //if(data.content.length != 0){
	                	console.log( "getAllQuestion:"+JSON.stringify(data));
	            	   $.each(data.content,function(i,data){
	            		
	            		//if(data.question.length != 0){
	            			//console.log( "getRetailerDashBoardData data.content.question:"+JSON.stringify(data.question));
		            		var qs = '<a href="javascript:getQuestionDetail(\''+data.id +'\')" class="list-group-item">'+ data.questionText+'</a>';		  
		            		$("#qsset").append(qs);
	            		//}
	      			});
            	//}
               //}
            },
            error: function (xhr, textStatus, errorThrown)
            {
            	var responseText;
            	alert('xhr.responseText :'+xhr.responseText);
            	try {
                    responseText = jQuery.parseJSON(xhr.responseText);
                } catch (e) {
                    responseText = xhr.responseText; 
                }
                alert('responseText :'+responseText);
            	alert("getcities :"+jqXHR+":"+textStatus+":"+errorThrown);
            	console.log( "getcities :"+jqXHR+":"+textStatus+":"+errorThrown);
            }
        });
		 
		
		//
	});
	</script>
</body>
</html>